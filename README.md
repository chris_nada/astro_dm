# Exoplanet-DM/KI Applikation "Exominer"

## Download
### Release 201811 für 64 bit Windows (23MB)
+ [Mirror 1](https://drive.google.com/file/d/1rTvae-EtHrIf3WQra29FxGwCGT-QlRSN)

## Screenshots
Im [Wiki](https://gitlab.com/chris_nada/astro_dm/wikis/screenshots).
![table_hosts](/uploads/655811203c74459aa15fda3bc6de8a8d/table_hosts.png)
![machine_learning](/uploads/91545bb923b80e6000b062907a214ef2/machine_learning.png)
![3d](/uploads/01699945642afc33bed0b8f2abe6e0b7/3d.png)


## Kontext

Data Mining ist keine Technologie, die den Physikern, in diesem Fall Astrophysikern, völlig fremd ist. 
Trotzdem tut sich diese Forschungsgemeinschaft eher schwer darin, insbesondere KI-Algorithmen bei der Analyse der Daten zu akzeptieren und zu verwenden, 
was auch durch eine geringe Anzahl Publikationen dieser Thematik repräsentiert wird.. Die allermeisten Astronomen sind immernoch Fans traditioneller, 
zweidimensioneller Diagramme, welche man überall in Veröffentlichungen und 
Internetpräsenzen vorfindet (z.B. http://exoplanet.eu/diagrams/, https://exoplanetarchive.ipac.caltech.edu/exoplanetplots/ ). 
Solche Diagramme haben durchaus ihre Daseinsberechtigung, können Zusammenhänge mit zwei Parametern intuitiv veranschaulichen. 
Um komplexere Zusammenhänge, die möglicherweise viele, wenn nicht alle, Parameter einschließen zu überblicken, reicht der Blick 
mit dem menschlichen Auge auf Plots höchstwahrscheinlich nicht aus.
Diesen Sachverhalt sieht dieses Projekt als Opportunität an, zu experimentieren, ob und inwiefern sich KI-Algorithmen auf 
missing Data in Exoplanetbeobachtungen anwenden lassen.

## Perspektive

Natürlich ist Exominer keine Anwendung mit nur Textausgabe: Auch Hobbyastronomen sollen mit der Software in der Lage sein, zu arbeiten. 
In dieser Hinsicht soll die Software es ermöglichen, trotz lückenhafter Beobachtungsdaten als Endergebnis mit Hilfe von Maschinenlernen, 
welche die Parameter vervollständen wird, ein (fast) beliebiges Planetensystem in 3D zu simulieren.
Darüberhinaus können auch andere aus den Beobachtungsdaten fehlende Werte durch Maschinenlernen ergänzt werden und mittels geläufiger
Formate (VOTable, CSV) zu anderen Anwendungen portiert werden.

## Weiterentwicklung

+ Analyse, inwiefern Forschungsergebnisse der [Arecibo Universität](http://phl.upr.edu/projects/habitable-exoplanets-catalog/data/database)
  einfließen können.
  + Die Daten sind so vollständig, sodass sie, sobald eine Anbindung an den Exominer besteht, Maschinenlernalgorithmen
    trainieren könnten, die umgehend für brandneue von der NASA veröffentliche Daten vervollständigen könnten.
+ Momentan sind nur zwei echte Maschinenlernalgorithmen implementiert, die jeweils einen Hostparameter vorhersagen.
  Hier gilt es, weitere Algorithmen einzubinden, die nicht nur Exoplanetenparameter vorhersagen können, sondern
  insbesondere auch Zusammenhänge zwischen Host und Exoplaneten erlernen.
+ [Langfristig] Die 3D-Systemansicht ist optisch aufzuwerten.
+ [Langfristig] Es existieren riesige Datenbanken von Sternen (von denen noch keine Trabanden bekannt sind). Mit Hilfe
  einer ausführlichen Datenbasis ließen sich (spekulative) Aussagen
  über eine Aufstellung möglicher Begleiter machen.

### Sonstiges
  + Caching wird noch nicht vollständig unterstützt

## (Aktuelle) Features 

+ Einlesen aktuellster veröffentlichter Exoplanetenparameter vom [NASA-Server](https://exoplanetarchive.ipac.caltech.edu/index.html).
+ Als Ergänzungsquelle wird [PHL](http://phl.upr.edu/projects/habitable-exoplanets-catalog/data/database) unterstützt.
+ Nutzen der aktuellen Stern bzw. Host-Datenbasis von [SIMBAD](http://simbad.u-strasbg.fr/simbad/) eine von der Universität Straßburg betriebene, allgemeine Objektdatenbank.
+ Tabellarisches Darstellen der Daten in einem UI.
+ Anwendung von Maschinenlernalgorithmen auf *missing data*.
+ Filtern der Daten zur Suche von Planetensystemen etc., die nutzergegebene Kriterien erfüllen im UI.
+ 3D Simulation / Orbitaldarstellung eines beliebigen Planetensystems aus den geladenen Daten, 
  für das entweder genügend Informationen bereits vorhanden sind oder die KI diese Vorhersagen treffen konnte.
+ Technik;
    + Programmiersprache C++ 17 (Kompiliert wird für Windows und Linux via [CMake](https://cmake.org/))
    + UI Bibliothek + 3D Engine: [Irrlicht](http://irrlicht.sourceforge.net/)
    + [DLib](http://dlib.net/)
    + [cURL](https://curl.haxx.se/)

## Über
Der Name Exominer ist ein Kofferwort aus Exoplanet + Miner (von Data-Mining) und außerdem eine Anspielung auf engl. examiner (jmd, der etw. untersucht).

## Lizenz
Open-Source unter [BSD-2](LICENSE).