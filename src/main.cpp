#include "ui/hauptfenster/hauptfenster.h"
#include "daten/io/simbad.h"
#include "daten/io/config.h"
#include "daten/io/net.h"
#include "daten/io/sync.h"

/// Programmeintrittspunkt
int main(int argc, char** argv) {

    // (Noch) nicht genutzt
    (void) argc;
    (void) argv;

    // Benötigte Verzeichnisse erstellen
    Sync::ordner_erstellen();

    // Konfiguration einlesen
    Simbad::URL1 = (Config::get_string("SIMBAD_URL1").empty()) ? Simbad::URL1 : Config::get_string("SIMBAD_URL1");
    Simbad::URL2 = (Config::get_string("SIMBAD_URL2").empty()) ? Simbad::URL2 : Config::get_string("SIMBAD_URL2");
    Net::PROXY   = (Config::get_string("PROXY").empty())       ? Net::PROXY   : Config::get_string("PROXY");

    // Nutzeroberfläche starten
    UI::init();
    { Hauptfenster hf; }
    UI::finalize();

    return 0;
}
