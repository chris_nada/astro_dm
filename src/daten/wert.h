#pragma once

#include <ostream>
#include <dlib/serialize.h>

/// Interface gemeinsamer Methoden von der Klasse `Wert`.
class Resettable {

public:

    /// Funktionstemplate zur Verwendung in erbenden Klassen.
    virtual void reset() = 0;

    /// Funktionstemplate zur Verwendung in erbenden Klassen.
    virtual std::string get_bytes() const = 0;

};

/**
 * Um die Werte physikalischer Eigenschaften zu speichern dient dieses Template.
 * Kann als Wrapper verstanden werden.
 * @tparam T Datentyp des zu speichernden Werts.
 */
template <typename T>
class Wert final : public Resettable {

public:

    /// ctor default.
    Wert() : wert(T()), bekannt(false), vorhersage(false) {}

    /// ctor für bekannten `Wert` aus gegebenem `T`.
    //explicit Wert(T wert) : Wert() { set_wert_data(wert); }

    /// copy-ctor
    Wert(const Wert<T>& rhs) {
        wert            = rhs.wert;
        einheit         = rhs.einheit;
        bekannt         = rhs.bekannt;
        vorhersage      = rhs.vorhersage;
        beschreibung    = rhs.beschreibung;
        wert_als_string = rhs.wert_als_string;
    }

    /// Ctor zum Deserialisieren
    Wert(const std::string& daten) {
        std::stringstream ss(daten);
        dlib::deserialize(wert, ss);
        dlib::deserialize(einheit, ss);
        dlib::deserialize(bekannt, ss);
        dlib::deserialize(vorhersage, ss);
        dlib::deserialize(beschreibung, ss);
        dlib::deserialize(wert_als_string, ss);
    }

    /// Methode die den Wert zu Bytes serialisiert.
    std::string get_bytes() const override {
        std::stringstream ss;
        try {
            dlib::serialize(wert, ss);
            dlib::serialize(einheit, ss);
            dlib::serialize(bekannt, ss);
            dlib::serialize(vorhersage, ss);
            dlib::serialize(beschreibung, ss);
            dlib::serialize(wert_als_string, ss);
        } catch (std::exception& e) {
            std::cerr << "Fehler in Wert::get_bytes() T=" << typeid(T).name() << ": " << e.what() << std::endl;
        }
        return ss.str();
    }

    /// Setzt den Wert als Vorhersage.
    inline void set_wert_ki(T wert) {
        Wert::wert  = wert;
        bekannt     = true;
        vorhersage  = true;
    }

    /// Setzt den Wert als bekannt aus Daten (für nicht-`std::string`s).
    void set_wert_data(T wert) {
        Wert::wert = wert;
        bekannt    = true;
        vorhersage = false;
    }

    /// Setzt den Wert als bekannt aus Daten (für `std::string`s).
    template<typename = typename std::enable_if<std::is_same<T, std::string>::value>>
    void set_wert_data(T wert) {
        if (!wert.empty()) {
            Wert::wert = wert;
            bekannt    = true;
            vorhersage = false;
        }
    }

    /// Setzt den Wert als bekannt aus einer 3rd-party Quelle (um Eigen-/Fremdvorhersagen zu unterscheiden).
    inline void set_wert_third_party(T wert, bool vorhersage) {
        Wert::wert       = wert;
        Wert::vorhersage = vorhersage;
        bekannt          = true;
    }

    /// Setzt den Wert (Zahl) als Bekannt aus einem Rohdatenstring (über eine temporäre double aus `std::stod(...)`).
    inline void set_wert_aus_datastring(const std::string& string) {
        if (!string.empty()) {
            try {
                T temp = static_cast<T>(std::stod(string));
                set_wert_data(temp);
            } catch (std::exception& e) { /* Macht nix */ }
        }
    }

    /// Ist der Wert bekannt?
    inline bool is_bekannt() const { return bekannt; }

    /// Ist der Wert eine Vorhersage?
    inline bool is_vorhersage() const { return vorhersage; }

    /// Liefert die Einheit des Werts als String.
    inline const std::string& get_einheit() const { return einheit; }

    /// Liefert eine Beschreibung des Werts.
    inline const std::string& get_beschreibung() const { return beschreibung; }

    /// Setter für die Einheit.
    inline void set_einheit(const std::string& einheit) { Wert::einheit = einheit; }

    /// Setter für die Beschreibung.
    inline void set_beschreibung(const std::string& beschreibung) { Wert::beschreibung = beschreibung; }

    /// Liefert den Wert.
    inline const T operator()() const { return wert; }

    /// Operator: Vergleicht nur `wert` auf Gleichheit.
    inline bool operator==(const Wert& rhs) const { return wert == rhs.wert; }

    /// Operator: Vergleicht nur `wert` auf nicht-Gleichheit.
    inline bool operator!=(const Wert& rhs) const { return !(rhs == *this); }

    /// Operator: Vergleicht nur `wert`.
    inline bool operator<(const Wert& rhs) const { return wert < rhs.wert; }

    /// Operator: Vergleicht nur `wert`.
    inline bool operator>(const Wert& rhs) const { return rhs < *this; }

    /// Operator: Vergleicht nur `wert`.
    inline bool operator<=(const Wert& rhs) const { return !(rhs < *this); }

    /// Operator: Vergleicht nur `wert`.
    inline bool operator>=(const Wert& rhs) const { return !(*this < rhs); }

    /// Operator: Streamausgabe. Nur `wert` wird in den Stream geschrieben.
    inline friend std::ostream& operator<<(std::ostream& os, const Wert& wert) {
        if (wert.bekannt) os << wert.wert;
        return os;
    }

    /// Operator: Bool-Konversion. Wert kann so in `if`-Abfragen vereinfacht genutzt werden.
    inline explicit operator bool() const { return bekannt; }

    /// Liefert den Wert als `std::string`. Leer (""), wenn unbekannt.
    inline std::string str() const {
        if (!bekannt) return std::string("");
        return std::to_string(wert);
    }

    /// Liefert den Wert als C-String. Leer (""), wenn unbekannt.
    inline const char* c_str() const {
        wert_als_string = str();
        return wert_als_string.c_str();
    }

    /// Setzt den Wert, falls es sich um eine Vorhersage handelt, zurück.
    inline void reset() override {
        if (vorhersage) {
            bekannt     = false;
            vorhersage  = false;
            wert = T();
        }
    }

private:

    /// Von der Klasse gekapselter Wert.
    T wert;

    /// Bezeichnung der Einheit.
    std::string einheit;

    /// Beschreibung.
    std::string beschreibung;

    /// Ist der Wert bekannt? Nein bedeutet, `wert` ist nicht aussagekräftig, obwohl vorhanden.
    bool bekannt;

    /// Ist der Wert eine Vorhersage?
    bool vorhersage;

    /// Speicher für temporären String des Werts.
    mutable std::string wert_als_string;

};