#include "planet.h"

Planet::Planet() : quelle(NASA) {
    // nichts weiter zu tun
}

std::vector<Planet*> Planet::get_planeten_sonnensystem() {
    std::vector<Planet*> sonnensystem_planeten;

    static const uint8_t N = 8; // Anzahl Planeten

    // 0 = Merkur, 1 = Venus, 2 = Erde, 3 = Mars, 4 = Jupiter, 5 = Saturn, 6 = Uranus, 7 = Neptun
    const char* name[]       = {"Merkur", "Venus", "Erde", "Mars", "Jupiter", "Saturn", "Uranus", "Neptun"};
    const char letter[]      = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
    const double orbit_p[]   = {87.95721484, 224.670039352, 365.20635448,  686.88554352,
                                4332.226998, 10754.2262696, 30682.9521592, 60181.790064};
    const double orbit_a[]   = {0.387, 0.723, 1.0, 1.524, 5.203, 9.537, 19.191, 30.069};
    //orbit_t TODO
    //orbit_w TODO
    const double orbit_i[]   = {7.005, 3.3947, 0.0, 1.851, 1.305, 2.484, 0.77, 1.769};
    const double orbit_e[]   = {0.20563069, 0.00677323, 0.01671022, 0.09341233,
                                0.04839266, 0.05415060, 0.04716771, 0.00858587};
    const double m[]         = {0.0553, 0.815, 1.0, 0.107, 317.83, 95.159, 14.536, 17.147};
    const double r[]         = {0.3825, 0.9488, 1.0, 0.53260, 11.209, 9.449, 4.007, 3.883};
    const double dichte[]    = {5.427, 5.243, 5.515, 3.933, 1.326, 0.687, 1.270, 1.638};
    const double temp[]      = {220, 730, 287, 227, 152, 134, 76, 73};
    const uint16_t n_monde[] = {0, 0, 1, 2, 69, 62, 27, 14};

    // Planeten erzeugen
    for (uint8_t i = 0; i < N; ++i) {
        Planet* planet = new Planet();
        planet->name = name[i];
        planet->host_name = "Sonne";
        planet->data().letter.set_wert_data(letter[i]);
        planet->data().orbit_p.set_wert_data(orbit_p[i]);
        planet->data().orbit_a.set_wert_data(orbit_a[i]);
        planet->data().orbit_i.set_wert_data(orbit_i[i]);
        planet->data().orbit_e.set_wert_data(orbit_e[i]);
        planet->data().m.set_wert_data(m[i]);
        planet->data().r.set_wert_data(r[i]);
        planet->data().dichte.set_wert_data(dichte[i]);
        planet->data().temp.set_wert_data(temp[i]);
        planet->data().n_monde.set_wert_data(n_monde[i]);
        sonnensystem_planeten.push_back(planet);
    }
    return sonnensystem_planeten;
}

void Planet::sonnensystem_laden() {
    auto sonnensystem = get_planeten_sonnensystem();
    for (auto planet : sonnensystem) planeten.push_back(planet);
}
