#pragma once

#include "../host/host.h"
#include "planetwerte.h"

#include <map>

class PHL;

/**
 * Datenspeicher für Exoplanetobjekte.
 */
class Planet final {

    friend class PHL;

public:

    /// Datenquelle.
    typedef enum Quelle { NASA, PHL } Quelle;

    /// Standardkonstruktor. Setzt Quelle auf NASA.
    Planet();

    /// Liefert alle Planeten als Pointerliste, die im Speicher sind.
    static const std::vector<Planet*>& alle() { return planeten; }

    /// Liefert alle Namen von Hosts, die nicht in der Host-map gefunden wurden.
    static std::vector<std::string> get_fehlende_hosts();

    /// Liest und parst die lokal gespeicherten NASA-Planeten CSV-Daten aus.
    static bool einlesen();

    /// Fügt das Sonnensystem hinzu, falls keine Planeten eingelesen sind.
    static void sonnensystem_laden();

    /// Getter: Name.
    const std::string& get_name() const { return name; }

    /// Getter: Hostname.
    const std::string& get_host_name() const { return host_name; }

    /// Getter: Datenquelle.
    Quelle get_quelle() const { return quelle; }

    /// Getter: `Planetwerte` Parameter const.
    const Planetwerte& data() const { return daten; }

    /// Getter: `Planetwerte` Parameter.
    Planetwerte& data() { return daten; }

private:

    /* NASA CSV-Spaltenbezeichnungen */
    static constexpr const char* const ID_NAME      = "mpl_name";
    static constexpr const char* const ID_LETTER    = "mpl_letter";
    static constexpr const char* const ID_HOST_NAME = "mpl_hostname";
    static constexpr const char* const ID_ORBIT_P   = "mpl_orbper";
    static constexpr const char* const ID_ORBIT_T   = "mpl_orbtper";
    static constexpr const char* const ID_ORBIT_W   = "mpl_orblper";
    static constexpr const char* const ID_ORBIT_A   = "mpl_orbsmax";
    static constexpr const char* const ID_ORBIT_E   = "mpl_orbeccen";
    static constexpr const char* const ID_ORBIT_I   = "mpl_orbincl";
    static constexpr const char* const ID_DICHTE    = "mpl_dens";
    static constexpr const char* const ID_TEMP      = "mpl_eqt";
    static constexpr const char* const ID_MONDE     = "mpl_mnum";
    static constexpr const char* const ID_MASSE     = "mpl_masse";
    static constexpr const char* const ID_MASSE_J   = "mpl_bmassj";
    static constexpr const char* const ID_MASSE_J2  = "mpl_massj";
    static constexpr const char* const ID_RADIUS_E  = "mpl_rade";
    static constexpr const char* const ID_RADIUS_J  = "mpl_radj";
    static constexpr const char* const ID_RADIUS_S  = "mpl_rads";
    static constexpr const char* const ID_DATUM     = "mpl_publ_date";
    static constexpr const char* const ID_METHODE   = "mpl_discmethod";

    /// Parst eingelesene Daten (`rohdaten`) zu `Planetwerte daten`.
    static void parsen();

    /// Debug: Wieviele Attribute sind mit jeweiliger ID in den Daten vorhanden.
    static void debug_attribut_anzahl();

    /// Gibt an, wieviele Planeten das Attribut aus dem Liste `daten_ids` gesetzt haben.
    static unsigned int attribut_anzahl(unsigned int pos);

    /// Speicher von Zeigern von allen geladenen Planeten.
    static inline std::vector<Planet*> planeten;

    /// CSV Überschriften.
    static inline std::vector<std::string> daten_ids;

    /// Gibt an in welcher Spalte sich Daten-ID `key` befindet.
    static inline std::map<std::string, uint16_t> daten_id_i;

    /// Enthält Beschreibungstexte zu den Daten-IDs. TODO
    static inline std::vector<std::string> daten_infos;

    /**
     * Liefert hartkodiert die 8 Planeten des Sonnensystems.
     * Quellen der verwendeten Parameter:
     * - https://en.wikipedia.org/wiki/List_of_gravitationally_rounded_objects_of_the_Solar_System
     * - http://www.astronomynotes.com/tables/tablesb.htm
     * @return Liste von via `new` erzeugten Planeten des Sonnensystems aufsteigent im Abstand zur Sonne.
     */
    static std::vector<Planet*> get_planeten_sonnensystem();

    /// Liefert die Rohdaten mit gegebenem Key als String.
    const inline std::string& get(const std::string& attribut) { return rohdaten[daten_id_i[attribut]]; }

    /// Ergänzt Daten zu diesem Planeten aus einem weiteren 'Rohdaten-`std::vector` (`rohdaten`).
    void daten_ergaenzen(const std::vector<std::string>& daten2);

    /// Name des Planeten.
    std::string name;

    /// Name des Hosts.
    std::string host_name;

    /// Ungeparste Rohdaten aus der CSV als String.
    std::vector<std::string> rohdaten;

    /// Geparste Daten des Planeten.
    Planetwerte daten;

    /// Datenquelle.
    Quelle quelle;

};
