#include "planetwerte.h"

std::map<std::string, std::string> Planetwerte::als_map() const {
    std::map<std::string, std::string> map;
    //map["Letter"]                 = std::string(letter());
    map["Orbitale Periode"]        = orbit_p.str();
    map["Periapsenzeit"]           = orbit_t.str();
    map["Periapsenargument"]       = orbit_w.str();
    map["Grosse Halbachse"]        = orbit_a.str();
    map["Exzentrizitaet"]          = orbit_e.str();
    map["Inklination"]             = orbit_i.str();
    map["Erdmassen"]               = m.str();
    map["Erdradien"]               = r.str();
    map["Dichte [g/cm3]"]          = dichte.str();
    map["Temperatur [K]"]          = temp.str();
    map["Entdeckungsmethode"]      = d_methode();
    map["Entdeckungsjahr"]         = std::to_string(d_datum()[0]);
    map["Temperaturklasse"]        = klasse_temp();
    map["Massenklasse"]            = klasse_mass();
    map["Chemischer Aufbau"]       = klasse_chem();
    map["Atmosphaerischer Aufbau"] = klasse_atmo();
    map["Habitabilitaetsklasse"]   = klasse_habi();
    return map;
}
