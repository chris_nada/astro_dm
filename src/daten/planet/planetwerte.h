#pragma once

#include "../wert.h"

/**
 * Speicher für die *rein physikalischen Eigenschaften* eines Planeten.
 * Beschreibungen:
 * https://exoplanetarchive.ipac.caltech.edu/docs/API_exomultpars_columns.html
 */
class Planetwerte final {

public:

    /// Planetenbuchstabe [Minuskel a, b, c...].
    Wert<char> letter;

    /// Orbitale Periode [d].
    Wert<double> orbit_p;

    /// Periapsiszeit t [d].
    Wert<double> orbit_t;

    /// Argument der Periapsis ω [°].
    Wert<double> orbit_w;

    /// Orbit Große Halbachse α [AU].
    Wert<double> orbit_a;

    /// Exzentrizität e.
    Wert<double> orbit_e;

    /// Inklination i [°].
    Wert<double> orbit_i;

    /// Masse m [Erdmassen].
    Wert<double> m;

    /// Radius r [Erdradien]. (mpl_ratror)
    Wert<double> r;

    /// Mittlere Dichte [g/cm³].
    Wert<double> dichte;

    /// Planet Equilibriumtemperatur [K].
    Wert<unsigned int> temp;

    /// Anzahl Monde.
    Wert<uint16_t> n_monde;

    /// Entdeckungsmethode.
    Wert<std::string> d_methode;

    /// Entdeckungszeitpunkt [YYYY, MM].
    Wert<std::array<unsigned short, 2>> d_datum;

    /// PHL - Temperaturklasse.
    Wert<std::string> klasse_temp;

    /// PHL - Massenklasse.
    Wert<std::string> klasse_mass;

    /// PHL - chemische Zusammensetzung.
    Wert<std::string> klasse_chem;

    /// PHL - atmosphärische Klasse.
    Wert<std::string> klasse_atmo;

    /// PHL - Habitabilität.
    Wert<std::string> klasse_habi;

    /// Liefert alle Werte als mbstring-map mit dazugehörigen Bezeichnungen auf Deutsch als Key.
    std::map<std::string, std::string> als_map() const;

};
