#include "planet.h"
#include "../io/sync.h"
#include "../io/etc.h"
#include "../io/net.h"
#include "../host/host.h"
#include "../konstante.h"

#include <iostream>
#include <dlib/string.h>

bool Planet::einlesen() {
    std::cout << "Planet::einlesen()" << std::endl;
    if (std::ifstream in(Sync::DATEI_PLANETEN); in.good()) {
        std::stringstream ss;
        ss << in.rdbuf();

        // Erste Zeile = Spaltenbezeichnungen
        {
            std::string erste_zeile;
            std::getline(ss, erste_zeile);
            daten_ids.clear();
            daten_id_i.clear();
            for (const std::string& token : Etc::tokenize(erste_zeile, ',')) {
                daten_id_i[token] = static_cast<uint16_t>(daten_ids.size());
                daten_ids.push_back(token);
            }
        }

        // Planetenliste leeren
        for (auto p : planeten) if (p) { delete p; p = nullptr; }
        planeten.clear();

        // Alle weiteren Zeilen auslesen
        std::cout << "\tDaten_IDs.size()=" << daten_ids.size() << std::endl;
        for (std::string zeile; std::getline(ss, zeile);) {
            const std::vector<std::string>& tokens = Etc::tokenize(zeile, ',');
            if (tokens.size() == daten_ids.size()) { // Zeile gültig
                // Planet anlegen
                Planet* p_neu = new Planet();
                p_neu->rohdaten.resize(tokens.size());
                for (uint16_t i = 0; i < tokens.size(); ++i) p_neu->rohdaten[i] = tokens[i];

                // Wichtige Attribute zuweisen
                p_neu->name      = p_neu->get(ID_NAME);
                p_neu->host_name = p_neu->get(ID_HOST_NAME);

                // Duplikat?
                bool duplikat = false;
                for (auto planet : planeten) if (planet->name == p_neu->name) {
                    duplikat = true;
                    planet->daten_ergaenzen(tokens);
                    break;
                }
                if (!duplikat) planeten.push_back(p_neu);
                else delete p_neu;

            } else std::cerr << "\tZeile ungueltig=" << zeile << std::endl;
        }
    } else std::cerr << "\t" << "Nicht gefunden: " << Sync::DATEI_PLANETEN << std::endl;
    //debug_attribut_anzahl();
    parsen();

    // Hartkodiert die Planeten des Sonnensystems hinzufügen
    for (Planet* p : get_planeten_sonnensystem()) planeten.push_back(p);

    return planeten.size() > 8; // Mehr als nur Sonnensystem vorhanden?
}

void Planet::daten_ergaenzen(const std::vector<std::string>& daten2) {
    // Sicherheitscheck
    if (rohdaten.size() != daten2.size()) {
        std::cerr << name << " .merge(" << daten2.size() << ") size() ungleich != " << rohdaten.size() << '\n';
        return;
    }
    // Zusammenführen
    for (unsigned int i = 0; i < daten2.size(); ++i) {
        if (rohdaten[i].empty() && !daten2[i].empty()) rohdaten[i] = daten2[i];
    }
}

void Planet::debug_attribut_anzahl() {
    std::cout << "Planet::debug_attribut_anzahl(); planeten.size() = " << planeten.size() << std::endl;
    for (unsigned i = 0; i < daten_ids.size(); ++i) {
        std::cout << '\t' << daten_ids[i] << ": ";
        std::cout << 100 * attribut_anzahl(i) / planeten.size() << "%\n";
    }
}

unsigned int Planet::attribut_anzahl(unsigned int pos) {
    unsigned int vorhanden = 0;
    for (auto p : planeten) {
        // Zählen wie oft das Attribut einen Eintrag hat (nicht leer ist)
        if (!p->rohdaten.at(pos).empty()) vorhanden++;
    }
    return vorhanden;
}

std::vector<std::string> Planet::get_fehlende_hosts() {
    std::vector<std::string> fehlende_hosts; // Hosts, die gebraucht werden
    for (Planet* planet : planeten) {
        if (planet && !Host::existiert(planet->host_name) &&
            !(std::find(fehlende_hosts.begin(), fehlende_hosts.end(), planet->host_name) != fehlende_hosts.end())) {
            fehlende_hosts.push_back(planet->host_name);
        }
    }
    std::cout << "\tPlanet::get_fehlende_hosts().size() = " << fehlende_hosts.size() << std::endl;
    return fehlende_hosts;
}

void Planet::parsen() {
    std::cout << "Planet::parsen()" << std::endl;
    for (auto planet : planeten) {
        {   // Letter
            std::string letter(planet->rohdaten[daten_id_i[ID_LETTER]]);
            if (!letter.empty()) {
                dlib::tolower(letter);
                planet->daten.letter.set_wert_data(letter[0]);
            }
        }
        {   // Entdeckungsmethode.
            const std::string& temp(planet->rohdaten[daten_id_i[ID_METHODE]]);
            if (!temp.empty()) planet->daten.d_methode.set_wert_data(temp);
        }
        {   // Entdeckungsdatum (Format YYYY-MM).
            const std::string& temp(planet->rohdaten[daten_id_i[ID_DATUM]]);
            if (!temp.empty()) {
                std::vector<std::string> tokens = Etc::tokenize(temp, '-');
                if (tokens.size() == 2 && tokens[0].size() <= 4 && tokens[1].size() <= 2) {
                    try {
                        std::array<unsigned short, 2> datum = {
                            static_cast<unsigned short>(std::stoi(tokens[0])),
                            static_cast<unsigned short>(std::stoi(tokens[1]))
                        };
                        planet->daten.d_datum.set_wert_data(datum);
                    } catch (std::exception& e) { /* Datum ungültig - Kein Problem. */ }
                }
            }
        }
        // Orbitale Periode [d].
        planet->daten.orbit_p.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_P]]);
        // Periapsiszeit t [d].
        planet->daten.orbit_t.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_T]]);
        // Argument der Periapsis ω [°].
        planet->daten.orbit_w.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_W]]);
        // Orbit Große Halbachse α [AU].
        planet->daten.orbit_a.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_A]]);
        // Exzentrizität e.
        planet->daten.orbit_e.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_E]]);
        // Inklination i [°].
        planet->daten.orbit_i.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_ORBIT_I]]);
        // Mittlere Dichte [g/cm³].
        planet->daten.dichte.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_DICHTE]]);
        // Planet Equilibrium Temperatur [K].
        planet->daten.temp.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_TEMP]]);
        // Anzahl Monde.
        planet->daten.n_monde.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_MONDE]]);
        // Masse m [Erdmassen].
        planet->daten.m.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_MASSE]]);
        if (!planet->daten.m) {
            planet->daten.m.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_MASSE_J]]);
            if (!planet->daten.m) planet->daten.m.set_wert_aus_datastring(planet->rohdaten[daten_id_i[ID_MASSE_J2]]);
            if (planet->daten.m) planet->daten.m.set_wert_data(planet->daten.m() / Konstante::Me_JUPITER);
        }
        // Radius r [Erdradien].
        planet->daten.r.set_wert_aus_datastring(planet->rohdaten[planet->daten_id_i[ID_RADIUS_E]]);
        if (!planet->daten.r) {
            planet->daten.r.set_wert_aus_datastring(planet->rohdaten[planet->daten_id_i[ID_RADIUS_J]]);
            if (planet->daten.r) planet->daten.r.set_wert_data(planet->daten.r() / Konstante::Re_JUPITER);
            else {
                planet->daten.r.set_wert_aus_datastring(planet->rohdaten[planet->daten_id_i[ID_RADIUS_S]]);
                if (planet->daten.r) planet->daten.r.set_wert_data(planet->daten.r() / Konstante::Re_SONNE);
            }
        }
        planet->quelle = Quelle::NASA; // Quelle auf NASA setzen.
    }
}
