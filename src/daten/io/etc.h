#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <dlib/rand.h>

/**
 * Sonstige Methoden, die nirgendwo anders hingepasst haben.
 */
class Etc final {

public:

   /**
     * Liefert einen std::vector aus std::string, die aus einem Text durch einen Separator geteilt wurden.
     * @param text Aufzuteilender Text
     * @param token char, bei dem aufgeteilt wird
     * @return Textteile
     */
    static std::vector<std::string> tokenize(const std::string& text, const char token);

    /**
     * Ersetzt in einem String alles Auftreten eines gegebenenn String durch einen neuen String.
     * @param text Zu modifizierender Text.
     * @param alt Zu ersetzende Strings.
     * @param neu Einzusetzende Strings.
     */
    static void replace_all(std::string& text, const std::string& alt, const std::string& neu);

    /// Öffnet gegebene URL im Standardbrowser. Kode Plattformabhängig. Implementiert für Windows, Linux.
    static void browser(std::string url);

    /**
     * Liefert eine Zufallszahl.
     * @tparam T Datentyp, muss ganzzahlig sein.
     * @param min Untere Grenze (einschließlich).
     * @param max Obere Grenze (einschließlich).
     * @return
     */
    template<typename T>
    static T zufall(T min, T max) {
        static dlib::rand rand(std::time(0));
        return static_cast<T>(rand.get_integer_in_range(min, max + 1));
    }

    /**
     * Entpackt eine zip-Datei im selbigen Pfad.
     * @param zip_datei Relativer oder absoluter Pfad zur zip-Datei.
     * @return Erfolgreich entpackt?
     */
    static bool entpacken(const std::string& zip_datei, const std::string& zielordner);

private:

    /// Instanzierung verboten.
    Etc() = delete;

};
