#include "../planet/planet.h"
#include "phl.h"
#include "etc.h"
#include "net.h"
#include "../konstante.h"

bool PHL::download() {
    std::cout << "PHL::" << __func__ << std::endl;
    (void) std::remove(ZIEL_PFAD); // evtl. vorhandene (veraltete) Datei löschen
    bool erfolg = Net::download(ZIP_URL, ZIEL_PFAD);
    if (erfolg) Etc::entpacken(ZIEL_PFAD, "data/hosts/phl");
    return erfolg;
}

bool PHL::einlesen() {
    std::cout << "PHL::" << __func__ << std::endl;
    if (std::ifstream in(CSV_DATEI); in.good()) {

        // (Für ausschließlich PHL gültige) Überschriften auslesen
        std::map<std::string, unsigned short> daten_ids;
        std::string zeile;
        std::getline(in, zeile);
        std::vector<std::string> tokens = Etc::tokenize(zeile, ',');
        for (unsigned short i = 0; i < tokens.size(); ++i) daten_ids[tokens[i]] = i;
        if (daten_ids.empty()) {
            std::cerr << "\tFehler: csv-header konnte nicht geparst werden. PHL nicht nutzbar." << std::endl;
            return false;
        }

        zaehler_neu = 0;
        zaehler_ergaenzt = 0;

        // Jede weitere Zeile = 1 Planet; auslesen
        for ( ;std::getline(in, zeile); ) {
            bool bestand = false;
            Planet* planet = nullptr;
            try {
                tokens = Etc::tokenize(zeile, ',');
                auto get_token = [&](const std::string& token) { return tokens.at(daten_ids.at(token)); };
                if (tokens.size() == daten_ids.size()) {

                    // Existiert Planet bereits?
                    std::string name(get_token(ID_NAME));
                    char letter = dlib::tolower(name).at(name.size() - 1); // Letter bestimmen
                    name = name.substr(0, name.size() - 2); // Namen bestimmen
                    for (Planet* planet_bestand : Planet::alle()) {
                        if (planet_bestand->data().letter() == letter) { // Letter gleich?
                            if (dlib::strings_equal_ignore_case(planet_bestand->get_name(), name) ||
                                dlib::strings_equal_ignore_case(planet_bestand->get_host_name(), name)) {
                                planet = planet_bestand; // gefunden; zuweisen
                                bestand = true;
                                break;
                            }
                        }
                    }
                    if (!planet) planet = new Planet(); // neu anlegen

                    // Daten auslesen (evtl. überschreiben)
                    planet->quelle = Planet::Quelle::PHL;
                    planet->name = name;
                    planet->host_name = name;
                    planet->data().letter.set_wert_data(letter);
                    planet->data().m.set_wert_aus_datastring(get_token(ID_MASSE));
                    planet->data().r.set_wert_aus_datastring(get_token(ID_RADIUS_E));
                    planet->data().temp.set_wert_aus_datastring(get_token(ID_TEMP));
                    planet->data().orbit_p.set_wert_aus_datastring(get_token(ID_ORBIT_P));
                    planet->data().orbit_w.set_wert_aus_datastring(get_token(ID_ORBIT_W));
                    planet->data().orbit_a.set_wert_aus_datastring(get_token(ID_ORBIT_A));
                    planet->data().orbit_e.set_wert_aus_datastring(get_token(ID_ORBIT_E));
                    planet->data().orbit_i.set_wert_aus_datastring(get_token(ID_ORBIT_I));

                    // Planetenklasse
                    planet->data().klasse_temp.set_wert_data(get_token(ID_CLASS_TEMP));
                    planet->data().klasse_atmo.set_wert_data(get_token(ID_CLASS_ATMO));
                    planet->data().klasse_chem.set_wert_data(get_token(ID_CLASS_COMP));
                    planet->data().klasse_habi.set_wert_data(get_token(ID_CLASS_HABI));
                    planet->data().klasse_mass.set_wert_data(get_token(ID_CLASS_MASS));

                    // Weitere Daten
                    if (!get_token(ID_DICHTE).empty()) { // Dichte umrechnen
                        double temp_dichte = std::stod(get_token(ID_DICHTE));
                        temp_dichte = temp_dichte / Konstante::DICHTE_ERDE;
                        planet->data().dichte.set_wert_data(temp_dichte);
                    }
                    if (!get_token(ID_METHODE).empty()) {
                        planet->data().d_methode.set_wert_data(get_token(ID_METHODE));
                    }
                    if (!get_token(ID_DATUM).empty()) {
                        planet->data().d_datum.set_wert_data(
                                { static_cast<unsigned short>(std::stoul(get_token(ID_DATUM))), 1 }
                        );
                    }

                    // Der Liste hinzufügen, zählen
                    if (!bestand) {
                        Planet::planeten.push_back(planet);
                        ++zaehler_neu;
                    } else ++zaehler_ergaenzt;
                } else {
                    std::cerr << "\tZeile ungueltig: " << tokens.size() << '/' << daten_ids.size();
                    std::cerr << ' ' << zeile << std::endl;
                }
            } catch (std::exception& e) {
                std::cerr << "\tZeile ungueltig: " << e.what() << " Zeile = " << zeile << std::endl;
                if (!bestand && planet) delete planet;
            }
        }
        std::cout << '\t' << zaehler_ergaenzt << " Planeten aus PHL-Quelle ergaenzt."     << std::endl;
        std::cout << '\t' << zaehler_neu      << " Planeten aus PHL-Quelle hinzugefuegt." << std::endl;
        return true;
    }
    std::cerr << CSV_DATEI << " nicht lesbar." << std::endl;
    return false;
}
