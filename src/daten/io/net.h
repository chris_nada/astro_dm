#pragma once

#include "../../ui/ladefenster.h"

#include <sstream>
#include <curl/curl.h>

/**
 * Helfermethoden zur Datenbeschaffung aus dem Internet.
 */
class Net final {

public:

    /// Lädt gegebene URL, gibt empfangene Antwort als String zurück. Leer bei Fehlschlag.
    static std::string download(const std::string& url);

    /// Lädt gegebene URL und speichert die Antwort in eine Datei mit gegebenem Namen. Gibt `0` bei Fehlschlag.
    static bool download(const std::string& url, const std::string& dateiname);

    /// Zu verwendener Proxyserver. Leer (`.empty()`) = kein Proxy verwenden.
    static inline std::string PROXY = "";

private:

    /// Erlaubt es, dass eine HTTP-Antwort aus cURL in einen `std::stringstream` geschrieben wird.
    static size_t write_to_stringstream(char* ptr, size_t anzahl, size_t einheit, void* daten);

    /// Von cURL vorgegebene Methode, die erlaubt, den Fortschritt des Herunterladens zu verfolgen.
    static int progress_callback(void *clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow);

    /// Beim Herunterladen verwendeter Statustext (Präfix).
    static inline irr::gui::IGUIStaticText* statustext1;

    /// Beim Herunterladen verwendeter Statustext (Dateninfo / Bytes).
    static inline irr::gui::IGUIStaticText* statustext2;

};