#include "net.h"
#include "../../ui/ui.h"

#include <iostream>
#include <fstream>

std::string Net::download(const std::string& url) {
    std::cout << "Net::download(" << url << ')' << std::endl;

    irr::core::stringw text_url(L"Empfange ");
    text_url += url.c_str();

    auto fenster_x = UI::driver->getScreenSize().Width;
    auto fenster_y = UI::driver->getScreenSize().Height;
    statustext1 = UI::guie->addStaticText(text_url.c_str(),
            { 10, (int)fenster_y - 32, 900, (int)fenster_y}, true);
    statustext2 = UI::guie->addStaticText(L"",
            {900, (int)fenster_y - 32, (int)fenster_x, (int)fenster_y}, true);
    statustext1->setOverrideFont(UI::FONT_MONO_12);
    statustext2->setOverrideFont(UI::FONT_MONO_16);

    // Curl Einstellungen
    CURL* curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.42.0");
    //curl_easy_setopt(curl, CURLOPT_MAXREDIRS, 8L);
    //curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

    // Streams, in die geschrieben wird
    std::stringstream ss_inhalt;
    std::stringstream ss_header;
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_to_stringstream);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ss_inhalt);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &ss_header);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0);
    curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_callback);
    if (!PROXY.empty()) curl_easy_setopt(curl, CURLOPT_PROXY, PROXY.c_str());
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);

    // Infos aus dem Header
    //char* content_type;
    //long status_code;
    //curl_off_t download_size = curl_off_t();
    //curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status_code);
    //curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &content_type);
    //curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD_T, &download_size);

    // Debug Infos ausgeben
    long curl_status = curl_easy_perform(curl);
    if (curl_status != 0) std::cerr << "\tCURL_STATUS = " << curl_status << std::endl;
    //std::cout << "\tSTATUS      = " << status_code             << '\n';
    //std::cout << "\tCONTENT     = " << content_type            << std::endl;

    // Aufräumen
    curl_easy_cleanup(curl);
    curl = nullptr;
    statustext1->remove();
    statustext2->remove();

    return ss_inhalt.str();
}

bool Net::download(const std::string& url, const std::string& dateiname) {
    #ifndef NDEBUG
        std::cout << "Net::download(" << url << ", " << dateiname << ")" << std::endl;
    #endif
    std::string content = download(url);
    if (!content.empty()) {
        std::ofstream out(dateiname, std::ios::binary);
        out << content;
        return true;
    }
    return false;
}

size_t Net::write_to_stringstream(char* ptr, size_t anzahl, size_t einheit, void* daten) {
    std::stringstream* ss = (std::stringstream*) daten;
    size_t groesse = anzahl * einheit;
    ss->write(ptr, groesse);
    return groesse;
}

int Net::progress_callback(void* clientp, curl_off_t dltotal, curl_off_t dlnow, curl_off_t ultotal, curl_off_t ulnow) {
    (void) clientp;
    (void) ultotal;
    (void) ulnow;
    (void) dltotal;
    //std::cout << "\tDownload: " << dlnow << '/' << dltotal << '\t' << ulnow << '/' << ultotal << '\n';

    static unsigned int update = 0;

    ++update;

    if (update == 0b0001'0000 /* 16 */) {
        statustext2->setText((std::to_wstring(dlnow / 1024) + L" KB").c_str());
        UI::driver->beginScene(true, true, UI::FARBE_HG);
        UI::guie->drawAll();
        UI::driver->endScene();
        update = 0x00;
    }

    return 0;
}
