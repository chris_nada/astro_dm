#pragma once

#include <string>

/**
 * Verwaltet das Arbeiten mit der Datenbasis des Planetary Habitability Laboratory der
 * Arecibo Universität.
 */
class PHL final {

public:

    /// Instanzierung explizit verboten, da global.
    PHL() = delete;

    /**
     * Lädt neueste verfügbare Datenbasis herunter.
     * @return Download erfolgreich?
     */
    static bool download();

    /**
     * Liest lokal vorhandene Datenbasis ein.
     * @note Bereits vorhandene Hosts und Exoplaneten werden um Daten ergänzt bzw. überschrieben.
     * @return Einlesen erfolgreich?
     */
    static bool einlesen();

    /// Gibt an, wieviele Planeten durch die PHL Quelle neu angelegt wurden.
    static inline unsigned int get_zaehler_neu() { return zaehler_neu; }

    /// Gibt an, wieviele Planeten durch die PHL Quelle in ihren Daten ergänzt wurden.
    static inline unsigned int get_zaehler_ergaenzt() { return zaehler_ergaenzt; }

private:

    /// URL zur verwendeten zip-Datei, die die Daten im csv-Format enthält.
    static inline constexpr const char* const ZIP_URL = "http://www.hpcf.upr.edu/~abel/phl/phl_hec_all_confirmed.csv.zip";

    /// Pfad, wo die heruntergeladene gepackte Datei gespeichert wird.
    static inline constexpr const char* const ZIEL_PFAD = "data/hosts/phl/phl_csv.zip";

    /// Dateiname der entpackten CSV-Datei.
    static inline constexpr const char* const CSV_DATEI = "data/hosts/phl/phl_hec_all_confirmed.csv";

    /// Anzahl hinzugefügter Planeten
    static inline unsigned int zaehler_neu = 0;

    /// Anzahl ergaenzter Planeten
    static inline unsigned int zaehler_ergaenzt = 0;

    /**
     * Überschriften (Header) der csv-Datei.
     * Vgl. http://phl.upr.edu/projects/habitable-exoplanets-catalog/data/database
     */
    static constexpr const char* const ID_NAME     = "P. Name";
    static constexpr const char* const ID_MASSE    = "P. Mass (EU)";
    static constexpr const char* const ID_RADIUS_E = "P. Radius (EU)";
    static constexpr const char* const ID_DICHTE   = "P. Density (EU)";
    static constexpr const char* const ID_TEMP     = "P. Ts Mean (K)";
    static constexpr const char* const ID_ORBIT_P  = "P. Period (days)";
    static constexpr const char* const ID_ORBIT_A  = "P. Sem Major Axis (AU)";
    static constexpr const char* const ID_ORBIT_E  = "P. Eccentricity";
    static constexpr const char* const ID_ORBIT_W  = "P. Omega (deg)";
    static constexpr const char* const ID_ORBIT_I  = "P. Inclination (deg)";
    static constexpr const char* const ID_METHODE  = "P. Disc. Method";
    static constexpr const char* const ID_DATUM    = "P. Disc. Year";

    static constexpr const char* const ID_CLASS_TEMP = "P. Zone Class";
    static constexpr const char* const ID_CLASS_MASS = "P. Mass Class";
    static constexpr const char* const ID_CLASS_COMP = "P. Composition Class";
    static constexpr const char* const ID_CLASS_ATMO = "P. Atmosphere Class";
    static constexpr const char* const ID_CLASS_HABI = "P. Habitable Class";

};