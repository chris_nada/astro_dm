#include "config.h"

void Config::set_pfad(const std::string& pfad) {
    Config::pfad = pfad;
}

std::string Config::get_string(const std::string& key) {
    std::ifstream in(pfad);
    if (in.good()) {

        // Zeilen durchgehen + am '=' teilen
        for (std::string zeile; std::getline(in, zeile);) {
            const std::vector<std::string>& tokens = Etc::tokenize(zeile, '=');

            // key gefunden?
            if (tokens.size() == 2 && tokens[0] == key) {
                std::cout << key << '=' << tokens[1] << '\n'; // DEBUG
                return tokens[1];
            }
        }
    }
    return "";
}

bool Config::get_bool(const std::string& key) {
    const std::string& wert = get_string(key);
    return !wert.empty() && wert[0] != '0';
}

template<typename T>
T Config::get_gleitkomma(const std::string& key) {
    try { return static_cast<T>(std::stod(get_string(key))); }
    catch (std::exception& e) { return T(); }
}

template<typename T>
T Config::get_ganzzahl(const std::string& key) {
    try { return static_cast<T>(std::stoll(get_string(key))); }
    catch (std::exception& e) { return T(); }
}

void Config::speichern(const std::map<std::string, std::string>& key_wert_paare) {
    for (const auto& paar : key_wert_paare) speichern(paar.first, paar.second);
}

bool Config::speichern(const std::string& key, const std::string& value) {
    bool key_gefunden = false; // Key bereits vorhanden?
    std::stringstream inhalt;  // Zu schreibender Inhalt

    // Bestehende Datei einlesen
    if (std::ifstream in(pfad); in.good()) {
        for (std::string zeile; std::getline(in, zeile);) {
            if (!key_gefunden && zeile.find(key) != std::string::npos) {
                zeile = (key + "=" + value);
                key_gefunden = true;
            }
            inhalt << zeile << '\n';
        }
    }
    if (!key_gefunden) inhalt << key << '=' << value << '\n';

    // Datei schreiben
    if (std::ofstream out(pfad); out.good()) {
        for (std::string zeile; std::getline(inhalt, zeile);) out << zeile << '\n';
        return true;
    }
    return false;
}

