#pragma once

#include "etc.h"

#include <map>

/**
 * Verwaltet das Speichern von Werten in einer Konfigurationsdatei.
 * + Das Format der Konfigurationsdatei ist eine unkomprimierte `ini`, mit `=` ohne Leerzeichen / Tabs
 * zeilenweise getrennter Schlüssel - Wertpaare.
 * + Praktisch kann jede `std::map<std::string, std::string>` gespeichert werde.
 * @note Schlüssel müssen einmalig sein.
 * @warning Die Methoden dürfen *nicht* parallelisiert verwendet werden.
 */
class Config final {

public:

    /**
     * Ändert den standardmäßig auf `config.ini` gesetzten Pfad zu einem neuen Wert.
     * Muss selbstverständlich vor dem Einlesen / Speichern reguliert werden.
     * @param pfad Neuer Pfad.
     **/
    static void set_pfad(const std::string& pfad);

    /**
     * Liefert aus der Konfigurationsdatei einen String.
     * @param key Schlüsselwert des Strings.
     * @return String aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Leerer String (`std::string.empty()`), falls nicht gefunden oder leerer Wert.
     */
    static std::string get_string(const std::string& key);

    /**
     * Liefert aus der Konfigurationsdatei eine `bool`. Bools werden als `0` oder `1` in die Datei geschrieben.
     * @param key Schlüsselwert der bool.
     * @return `bool` aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Leerer String (`std::string.empty()`), falls nicht gefunden oder leerer Wert.
     */
    static bool get_bool(const std::string& key);

    /**
     * Liefert aus der Konfigurationsdatei eine Gleitkommazahl.
     * @note Intern wird als Cast eine `double` verwendet.
     * @param key Schlüsselwert der Zahl.
     * @return Zu `T` gecastete Zahl aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Liefert `T()`, falls `key` nicht gefunden wird.
     */
    template <typename T>
    static T get_gleitkomma(const std::string& key);

    /**
     * Liefert aus der Konfigurationsdatei eine Ganzzahl.
     * @note Intern wird als Cast eine `long long` verwendet.
     * @param key Schlüsselwert der Zahl.
     * @return Zu `T` gecastete Zahl aus der Konfigurationsdatei, der hinter dem Schlüssel steht.
     * Liefert `T()`, falls `key` nicht gefunden wird.
     */
    template <typename T>
    static T get_ganzzahl(const std::string& key);

    /**
     * Speichert gegebene `std::map` als `ini`-Datei ab, aus der mittels der hier gegebenen
     * get-Methoden Werte wieder ausgelesen werden können.
     * @param key_wert_paare Zu speichernde Werte.
     * @note `bool` müssen als `0` oder `1` repräsentiert sein, damit sie korrekt wieder eingelesen werden.
     */
    static void speichern(const std::map<std::string, std::string>& key_wert_paare);

    /**
     * Speichert gegebenes Key-Value paar in der verwendeten Config-Datei ab.
     * @param key Schlüssel (links vom Gleichheitszeichen).
     * @param value Wert (rechts vom Gleichheitszeichen).
     */
    static bool speichern(const std::string& key, const std::string& value);

private:

    /// Von den Lese/Schreibmethoden verwendeter Pfad zur Konfigurationdatei.
    static inline std::string pfad = "config.ini";

};

