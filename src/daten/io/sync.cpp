#include "sync.h"
#include "net.h"
#include "../planet/planet.h"
#include "../host/host.h"
#include "phl.h"
#include "config.h"

#include <dlib/misc_api.h>

bool Sync::update() {
    std::cout << "Sync::update()" << std::endl;
    bool ergebnis = Net::download(URL_PLANETEN, DATEI_PLANETEN);
    ergebnis *= Net::download(URL_HOSTS, DATEI_HOSTS);
    ergebnis *= Planet::einlesen();
    ergebnis *= Host::einlesen(true);
    return ergebnis; // True, wenn alles geklappt hat
}

bool Sync::sync() {
    std::cout << "Sync::sync()" << std::endl;
    bool ergebnis = sync(DATEI_PLANETEN, URL_PLANETEN);
    ergebnis *= sync(DATEI_HOSTS, URL_HOSTS);
    ergebnis *= Planet::einlesen();
    if (!Host::cache_lesen()) ergebnis *= Host::einlesen();
    if (Config::get_bool("PHL_LESEN")) PHL::einlesen();
    return ergebnis; // True, wenn alles geklappt hat
}

bool Sync::sync(const std::string& datei, const std::string& url) {
    // Sync Planeten
    std::ifstream in(datei);
    if (!in.good()) {
        std::cout << '\t' <<  datei << " nicht vorhanden, lade herunter..." << std::endl;
        if (!Net::download(url, datei)) {
            std::cerr << '\t' << datei << " konnte nicht synchronisiert werden." << std::endl;
            return false;
        }
    }
    return true;
}

bool Sync::ordner_erstellen() {
    try {
        dlib::create_directory("data");
        dlib::create_directory("data/hosts");
        dlib::create_directory("data/hosts/phl");
        dlib::create_directory(Host::DIR_HOSTS_SIMBAD);
        dlib::create_directory("data/gfx");
        dlib::create_directory("data/gfx/thumbs");
        return true;
    } catch (std::exception& e) { return false; }
}
