#pragma once

#include <map>

/// Von SIMBAD verwendete Enumerators werden hier übersetzt.
class Simbad final {

public:

    /**
     * URL zum Server der SIMBAD-Datenbank. Standard ist `http://simbad.u-strasbg.fr/simbad`.
     */
    static std::string URL1;

    /**
     * SIMBAD Mirror. Wenn dieser genutzt wird (nicht leer ist), läuft das Herunterladen der Hosts parallel.
     * Standard ist `http://simbad.harvard.edu/simbad`.
     */
    static std::string URL2;

    /// URL, welche SIMBAD-OIDs annimmt, und Vorschaubilder der Objekte liefert.
    static constexpr const char* const URL_THUMBS = "http://alasky.u-strasbg.fr/cgi/simbad-thumbnails/get-thumbnail.py?oid=";

    /// http://simbad.u-strasbg.fr/simbad/sim-display?data=otypes
    static const std::map<std::string, std::string> OTYPE;

private:

    /// Instanzierung verboten.
    Simbad() = default;;

};