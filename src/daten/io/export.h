#pragma once

#include "../host/host.h"

/**
 * Enthält statische Methoden zum Export in geläufige Dateitypen von der Software verwendeter Daten.
 */
class Export final {

public:

    /**
     * Exportiert eine gegebene Liste an Hosts in eine CSV-Datei mit gegebenem Pfad.
     * @note Es sind schreibrechte auf den gegebenen Pfad notwendig.
     * @param hosts Liste zu schreibender Hosts.
     * @param pfad Dateipfad (inkl. Dateierweiterung), wohin geschrieben werden soll.
     * @return True, wenn die CSV-Datei geschrieben werden konnte; false wenn nicht.
     */
    static bool csv(const std::vector<Host*>& hosts, const std::string& pfad);

private:

    /// Instanzierung verboten.
    Export() = default;

};