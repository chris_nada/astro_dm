#include "export.h"

#include <iostream>
#include <fstream>

bool Export::csv(const std::vector<Host*>& hosts, const std::string& pfad) {
    std::cout << "Export::csv(" << hosts.size() << " Hosts, " << pfad << ") " << std::endl;

    // Erste Zeile der CSV-Datei - Header.
    static constexpr const char* const CSV_HEADER =
            "name;"
            "ra;dek;"
            "eigenbewegung_ra;eigenbewegung_dek;"
            "dist;multi;"
            "spektraltyp;spektraltyp_zusatz;leuchtkraftklasse;"
            "typ;temp;r;m;alter;";
    std::ofstream out(pfad);
    if (out.good()) {
        out << CSV_HEADER << '\n';

        // Daten für jeden Host eintragen
        for (Host* host : hosts) {
            const Hostwerte& hostwerte = host->data();
            out << host->get_name()             << ';';
            out << hostwerte.ra                 << ';';
            out << hostwerte.dek                << ';';
            out << hostwerte.eigenbewegung_ra   << ';';
            out << hostwerte.eigenbewegung_dek  << ';';
            out << hostwerte.dist               << ';';
            out << hostwerte.multi              << ';';
            out << hostwerte.spektraltyp        << ';';
            out << hostwerte.spektraltyp_zusatz << ';';
            out << hostwerte.leuchtkraftklasse  << ';';
            out << hostwerte.typ()              << ';';
            out << hostwerte.temp               << ';';
            out << hostwerte.r                  << ';';
            out << hostwerte.m                  << ';';
            out << hostwerte.alter              << ';';
            out << '\n';
        }
        return true;
    }
    return false;
}
