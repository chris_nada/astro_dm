#include <dlib/dir_nav.h>
#include "etc.h"

std::vector<std::string> Etc::tokenize(const std::string& text, const char token) {
    std::vector<std::string> teile;
    unsigned long long anfang = 0;
    unsigned long long ende = 0;
    while ((ende = text.find(token, anfang)) != std::string::npos) {
        teile.push_back(text.substr(anfang, ende - anfang));
        anfang = ende;
        anfang++;
    }
    teile.push_back(text.substr(anfang));
    return teile;
}

void Etc::replace_all(std::string& text, const std::string& alt, const std::string& neu) {
    if (alt.empty()) return;
    size_t start_pos = 0;
    while ((start_pos = text.find(alt, start_pos)) != std::string::npos) {
        text.replace(start_pos, alt.length(), neu);
        start_pos += neu.length();
    }
}

void Etc::browser(std::string url) {
    replace_all(url, " ", "%20"); // denn Leerzeichen nicht erlaubt
    std::cout << "Browser: " << url << std::endl;
    #if defined(_WIN32)
        std::string cmd("rundll32 url.dll,FileProtocolHandler " + url);
        system(cmd.c_str());
    #elif defined(__linux__)
        std::string cmd("xdg-open " + url + " &");
        std::cout << system(cmd.c_str()) << std::endl;
    #elif defined(__APPLE__)
        std::string cmd("open " + url);
        system(cmd.c_str());
    #else
        #error Etc::browser() fuer Plattform nicht definiert.
    #endif
}

bool Etc::entpacken(const std::string& zip_datei, const std::string& zielordner) {
    #if defined(_WIN32)
        try {
            //dlib::file zip_file(zip_datei);
            std::string cmd(R"(.\data\7zip\7za.exe e )" + zip_datei + " -aoa -o" + zielordner);
            std::cout << cmd << std::endl;
            system(cmd.c_str());
            return true;
        }
        catch (std::exception& e) {
            std::cerr << e.what() << std::endl;
            return false;
        }
    #elif defined(__linux__)
        std::string cmd("unzip " + zip_datei + " -d " + zielordner);
        std::cout << cmd << std::endl;
        system(cmd.c_str());
        return true;
    #else
        #error Etc::entpacken() fuer Plattform nicht definiert.
    #endif
    return false;
}

