#pragma once

#include <string>
#include <dlib/compress_stream.h>

/**
 * Klasse enthält statische Methoden zur Synchronisation der im Projekt verwendeten Daten.
 */
class Sync final {

public:

    /// Rein statische Klasse.
    Sync() = delete;

    /// Bei Kompression verwendeter Algorithmus.
    typedef dlib::compress_stream::kernel_1ea dlib_kompressor;

    /// Pfad zur Synchronisationsdatei für Planeten.
    static constexpr const char* const DATEI_PLANETEN = "data/planets.csv";

    /// Pfad zur Synchronisationsdatei für Sterne.
    static constexpr const char* const DATEI_HOSTS    = "data/hosts/hosts.csv";

    /// vgl. https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html
    static constexpr const char* const URL_PLANETEN   = "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=multiexopars&select=*";

    /// vgl. https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html
    static constexpr const char* const URL_HOSTS      = "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=missionstars&select=*";

    /// Führt ein Update durch, wobei `DATEI_PLANETEN` und `DATEI_STERNE` angelegt bzw. überschrieben wird.
    static bool update();

    /// Wie `update()`, allerdings werden die Dateien, falls vorhanden, belassen und nicht aktualisiert.
    static bool sync();

    /// Erzeugt, wenn nötig, vom Programm verwendete Verzeichnisse. Gibt `false`, wenn fehlgeschlagen.
    static bool ordner_erstellen();

private:

    /// Falls die Datei `datei` nicht vorhanden ist, wird sie von `url` heruntergeladen.
    static bool sync(const std::string& datei, const std::string& url);

};
