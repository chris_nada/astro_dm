#pragma once

/**
 * Enthält von der Software genutzte Konstanten.
 */
class Konstante final {

public:

    /// Radius der Sonne in Kilometern.
    static inline constexpr const unsigned int R_SONNE = 696'000;

    /// Radius der Sonne in Erdradien.
    static inline constexpr const double Re_SONNE = 109.0;

    /// Radius Jupiter in Erdradien.
    static inline constexpr const double Re_JUPITER = 10.517;

    /// Masse Jupiter in Erdenmassen.
    static inline constexpr const double Me_JUPITER = 317.82838;

    /// 1 pc entspricht x Lichtjahren.
    static inline constexpr const double LY_PC = 3.261563777;

    /// Dichte der Erde in g/cm³.
    static inline constexpr const double DICHTE_ERDE = 5.519;

private:

    /// Instanzierung verboten.
    Konstante() = delete;

};