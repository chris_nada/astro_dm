#pragma once

#include "host/host.h"
#include "planet/planet.h"

/**
 * Wrapper-Klasse, das ein Planetensystem samt seiner Host(s) enthält.
 * HP_System = Host-Planeten-System.
 */
class HP_System final {

public:

private:

    /// Zeiger zu allen Hosts des Systems.
    std::vector<Host*>   hosts;

    /// Zeiger zu allen Planeten des Systems.
    std::vector<Planet*> planeten;

};