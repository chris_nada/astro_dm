#pragma once

#include <irrString.h>
#include <iostream>
#include <dlib/matrix.h>

/**
 * Beinhaltet statische Methoden, die mithilfe von Maschinenlernalgorithmen unbekannte Hostparameter vorhersagen
 * und vervollständigen.
 */
class Host_KI final {

public:

    /// Für die von DLib verwendete Regression mögliche Kernels.
    enum KERNEL {
        RADIALBASIS,
        LINEAR,
        POLYNOMIAL,
        HISTOGRAMMINTERSEKTION,
        SIGMOID
    };

    /**
     * Führt alle verfügbaren Regressionsalgorithmen aus.
     * @return Mehrzeiliger, längerer Gesamtbericht in menschenlesbarer Form.
     */
    static irr::core::stringw los(KERNEL kernel);

private:

    /// Verwendeter Sample-Datentyp des ersten Algorithmus (Spektraltyp -> Temperatur).
    typedef dlib::matrix<double, 1, 1> matrix_A;

    /// Verwendeter Sample-Datentyp des zweiten Algorithmus (Spektraltyp + Temperatur -> Radius).
    typedef dlib::matrix<double, 2, 1> matrix_B;

    /**
     * Dieser Algorithmus schließt vom Spektraltyp auf die Temperatur.
     * @return Mehrzeiliger Statusbericht in menschenlesbarer Form.
     */
    template <typename kernel>
    static irr::core::stringw spektraltyp_zu_temp();

    /**
     * Dieser Algorithmus sagt anhand des Spektraltyps und der Temperatur auf den Radius vorraus.
     * @return Mehrzeiliger Statusbericht in menschenlesbarer Form.
     */
    template <typename kernel>
    static irr::core::stringw spektraltyp_und_temp_zu_r();

    /**
     * Sagt die Masse eines Sterns über das Stefan-Boltzmann-Gesetz, der Beziehung zwischen
     * Temperatur und Masse oder auch Radius und Masse voraus.
     * @return Einfache Aussage, wie oft der Algorithmus angewandt wurde.
     */
    static irr::core::stringw vorhersage_masse_boltzmann();

    /// Konvertiert für die KI einen Spektraltyp zur `double`.
    static double spektraltyp_als_double(char spektraltyp);

};
