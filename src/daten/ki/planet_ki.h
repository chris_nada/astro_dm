#pragma once

/**
 * Beinhaltet statische Methoden, die mithilfe von Maschinenlernalgorithmen unbekannte Parameter der
 * Planeten vorhersagen und vervollständigen.
 */
class Planet_KI final {

public:

    /// Führt alle verfügbaren Regressionsalgorithmen aus.
    static void los();

private:



};