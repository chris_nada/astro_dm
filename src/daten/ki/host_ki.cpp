#include "host_ki.h"
#include "regression.h"
#include "../host/host.h"

#include <numeric>

irr::core::stringw Host_KI::los(KERNEL kernel) {
    std::cout << "Host_KI::los()" << std::endl;
    irr::core::stringw bericht;

    // Temperaturvorhersage + Radiusvorhersage
    switch (kernel) {
        case RADIALBASIS:
            bericht += spektraltyp_zu_temp      <dlib::radial_basis_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::radial_basis_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
        case LINEAR:
            bericht += spektraltyp_zu_temp      <dlib::linear_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::linear_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
        case POLYNOMIAL:
            bericht += spektraltyp_zu_temp      <dlib::polynomial_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::polynomial_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
        case HISTOGRAMMINTERSEKTION:
            bericht += spektraltyp_zu_temp      <dlib::histogram_intersection_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::histogram_intersection_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
        case SIGMOID:
            bericht += spektraltyp_zu_temp      <dlib::sigmoid_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::sigmoid_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
        default:
            bericht += spektraltyp_zu_temp      <dlib::histogram_intersection_kernel<matrix_A>>(); bericht += L"\n\n";
            bericht += spektraltyp_und_temp_zu_r<dlib::histogram_intersection_kernel<matrix_B>>(); bericht += L"\n\n";
            break;
    }

    // Stefan-Boltzmann-Gesetz
    bericht += vorhersage_masse_boltzmann();
    return bericht;
}

template <typename kernel>
irr::core::stringw Host_KI::spektraltyp_zu_temp() {
    std::cout << "Host_KI::spektraltyp_zu_temp()" << std::endl;
    irr::core::stringw bericht(L"Regressionsanalyse: Temperatur");

    // KI init
    Regression<kernel, double, 1, 1> r;

    // Lerndaten befüllen
    for (Host* host : Host::alle_unique()) {
        if (host->data().spektraltyp.is_bekannt() && host->data().temp.is_bekannt()) {
            matrix_A m;
            double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
            if (host->data().spektraltyp_zusatz.is_bekannt()) spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
            m(0) = spektraltyp;
            r.add_lerndaten(m, host->data().temp());
        }
    }

    // Lerndaten vorhanden?
    if (r.size() > 0) {
        // Training
        r.training();

        // Test
        std::vector<double> abweichungen;
        for (Host* host : Host::alle_unique()) {
            if (host->data().spektraltyp.is_bekannt() && host->data().temp.is_bekannt()) {
                matrix_A m;
                double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
                if (host->data().spektraltyp_zusatz.is_bekannt())
                    spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
                m(0) = spektraltyp;
                abweichungen.push_back(abs(r.predict(m) - host->data().temp()));
            }
        }
        // Bericht generieren
        r.bericht_generieren(bericht, abweichungen);
    }
    else {
        // Keine Lerndaten
        bericht += L"\nAlgorithmus konnte nicht angewandt werden: Keine Daten.";
        return bericht;
    }

    // Gelerntes anwenden
    unsigned long long int angewandt = 0;
    for (Host* host : Host::alle_unique()) {
        if (host->data().spektraltyp.is_bekannt() && !host->data().temp.is_bekannt()) {
            matrix_A m;
            double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
            if (host->data().spektraltyp_zusatz.is_bekannt())
                spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
            m(0) = spektraltyp;
            host->data().temp.set_wert_ki(static_cast<uint32_t>(r.predict(m)));
            ++angewandt;
        }
    }
    bericht += L"Anzahl Vorhersagen = ";
    bericht += std::to_wstring(angewandt).c_str();
    return bericht;
}

template <typename kernel>
irr::core::stringw Host_KI::spektraltyp_und_temp_zu_r() {
    std::cout << "Host_KI::spektraltyp_und_temp_zu_r()" << std::endl;
    irr::core::stringw bericht(L"Regressionsanalyse: Radius");

    // KI init
    Regression<kernel, double, 2, 1> r;

    // Lerndaten befüllen
    for (Host* host : Host::alle_unique()) {
        if (host->data().spektraltyp.is_bekannt() &&
            host->data().temp.is_bekannt() &&
            host->data().r.is_bekannt()) {
            matrix_B m;
            double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
            if (host->data().spektraltyp_zusatz.is_bekannt()) spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
            m(0) = spektraltyp;
            m(1) = host->data().temp();
            r.add_lerndaten(m, host->data().r());
        }
    }

    // Lerndaten vorhanden?
    if (r.size() > 0) {
        // Training
        r.training();

        // Test
        std::vector<double> abweichungen;
        for (Host* host : Host::alle_unique()) {
            if (host->data().spektraltyp.is_bekannt() &&
                host->data().temp.is_bekannt() &&
                host->data().r.is_bekannt()) {
                matrix_B m;
                double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
                if (host->data().spektraltyp_zusatz.is_bekannt()) spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
                m(0) = spektraltyp;
                m(1) = host->data().temp();
                abweichungen.push_back(abs(r.predict(m) - host->data().r()));
            }
        }
        // Bericht generieren
        r.bericht_generieren(bericht, abweichungen);
    }
    else {
        // Keine Lerndaten
        bericht += L"\nAlgorithmus konnte nicht angewandt werden: Keine Daten.";
        return bericht;
    }

    // Gelerntes anwenden
    unsigned long long int angewandt = 0;
    for (Host* host : Host::alle_unique()) {
        if (host->data().spektraltyp.is_bekannt() &&
            host->data().temp.is_bekannt() &&
            !host->data().r.is_bekannt()) {
            matrix_B m;
            double spektraltyp = spektraltyp_als_double(host->data().spektraltyp());
            if (host->data().spektraltyp_zusatz.is_bekannt()) spektraltyp += host->data().spektraltyp_zusatz() * 0.1;
            m(0) = spektraltyp;
            m(1) = host->data().temp();
            host->data().r.set_wert_ki(r.predict(m));
            ++angewandt;
        }
    }
    bericht += L"Anzahl Vorhersagen = ";
    bericht += std::to_wstring(angewandt).c_str();
    return bericht;
}

irr::core::stringw Host_KI::vorhersage_masse_boltzmann() {
    std::cout << "Host_KI::vorhersage_masse_boltzmann()" << std::endl;
    irr::core::stringw bericht(L"Vorhersage der Masse nach Boltzmann-Gesetz wurde in ");
    unsigned long long int angewandt_r = 0;
    unsigned long long int angewandt_T = 0;
    for (Host* host : Host::alle_unique()) {
        if (!host->data().m.is_bekannt()) {

            // Radius bekannt? (Quelle: http://www.daviddarling.info/encyclopedia/M/mass-radius_relation.html)
            if (host->data().r.is_bekannt()) {
                host->data().m.set_wert_ki(pow(host->data().r(), 1.25)); // ^(5/4)
                ++angewandt_r;
            }
            // Temperatur bekannt? (Quelle: https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_law)
            else if (host->data().temp.is_bekannt()) {
                double x = host->data().temp();
                x /= 5778.0; // Temperatur Sonne
                x = x * x * x * x; // ^4
                x = pow(x, 0.4); // ^(2/5)
                host->data().m.set_wert_ki(x);
                ++angewandt_T;
            }
        }
    }
    // Bericht schreiben
    bericht += std::to_wstring(angewandt_r).c_str();
    bericht += L" Fällen über die Radius-Masse-Beziehung und in ";
    bericht += std::to_wstring(angewandt_T).c_str();
    bericht += L" Fällen über die Temperatur-Masse-Beziehung angewandt.";
    return bericht;
}

double Host_KI::spektraltyp_als_double(char spektraltyp) {
    switch (spektraltyp) {
        case ('O'): return 0.0;
        case ('B'): return 1.0;
        case ('A'): return 2.0;
        case ('F'): return 3.0;
        case ('G'): return 4.0;
        case ('K'): return 5.0;
        case ('M'): return 6.0;
        default:
            std::cerr << "\tHost_KI::spektraltyp_als_double(" << spektraltyp << ") undefiniert." << std::endl;
            return 5.0;
    }
}

