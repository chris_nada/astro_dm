#pragma once

#include <iostream>
#include <vector>
#include <dlib/svm.h>
#include <numeric>

/**
 * Maschinenlerntyp:
 * Regressionsanalyse: Kernel ridge regression
 *
 * @tparam sample_type Datentyp der Werte aus der Datenmatrix. In aller Regel ``double``.
 * @tparam spalten Anzahl der Spalten in der Datenmatrix.
 * @tparam zeilen Anzahl der Zeilen in der Datenmatrix.
 */
template<typename  kernel, typename sample_type, const long spalten, const long zeilen>
class Regression final {

public:

    /// Samplematrix.
    typedef dlib::matrix<sample_type, spalten, zeilen> matrix;

    /// Konstruktor.
    Regression() = default;

    /// Legt Gamma fest.
    void set_gamma(const double gamma) { trainer.set_kernel(kernel(gamma)); }

    /**
     * Legt Gamma automatisch fest nach der Formel 1.0 / (mittlere quadratische Distanz aller punkte).
     * Liefert erst sinnvolle Werte, nachdem der Lernkorpus eingegeben wurde.
     */
    void set_gamma() {
        const double gamma = 3.0 / dlib::compute_mean_squared_distance(dlib::randomly_subsample(samples, 2000));
        trainer.set_kernel(kernel(gamma));
        std::cout << "Regression::gamma = " << gamma << std::endl;
    }

    /// Trainingsdaten hinzufügen. sample = Ordinate, target = abhängige Variable.
    void add_lerndaten(const matrix sample, const sample_type target) {
        samples.push_back(sample);
        targets.push_back(target);
    }

    /// Training mit aktuellem Korpus durchführen
    void training() {
        std::cout << "Regression.training()" << std::endl;
        trainer.be_verbose();
        decision_function = trainer.train(samples, targets);
    }

    /// Vorhersage treffen zu gegebenem Wert.
    const sample_type predict(const matrix value) const {
        return decision_function(value);
    }

    /// Liefert die Größe des Lernkorpus.
    unsigned long long int size() const {
        return samples.size();
    }

    /**
     * Generiert aus berechneten Abweichungen einen Bericht über die Zuverlässigkeit des trainierten Algorithmus.
     * Berechnete Parameter der Abweichungen sind:
     * - Median
     * - Arithmetisches Mittel
     * - Maximum
     * @param bericht
     * @param abweichungen
     */
    void bericht_generieren(irr::core::stringw& bericht, std::vector<sample_type>& abweichungen) const {
        std::sort(abweichungen.begin(), abweichungen.end());
        bericht += L"\nAnzahl Lerndaten = ";
        bericht += std::to_wstring(size()).c_str();
        bericht += L"\nAbweichung, Median = ";
        bericht += std::to_wstring(abweichungen[abweichungen.size() / 2]).c_str();
        bericht += L"\nAbweichung, Arithmetisches Mittel = ";
        bericht += std::to_wstring(
                std::accumulate(abweichungen.begin(), abweichungen.end(), 0.0) / abweichungen.size()).c_str();
        bericht += L"\nAbweichung, Max = ";
        bericht += std::to_wstring(*std::max_element(abweichungen.begin(), abweichungen.end())).c_str();
        bericht += L"\n";
    }

private:

    /// Beinhaltet alle Lerndaten.
    std::vector<dlib::matrix<sample_type, spalten, zeilen>> samples;

    /// Beinhaltet alle Targets.
    std::vector<sample_type>                                targets;

    /// DLib-Trainerobjekt.
    dlib::krr_trainer<kernel>                               trainer;

    /// DLib-Entscheidungsfunktion. Kann nach dem Anlernen serialisiert werden.
    dlib::decision_function<kernel>                         decision_function;

};

