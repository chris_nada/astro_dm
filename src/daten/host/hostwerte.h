#pragma once

#include "../wert.h"

#include <cmath>
#include <sstream>
#include <vector>

/**
 * Speicher für die *rein physikalischen Eigenschaften* eines Hosts.
 * Beschreibungen:
 * https://exoplanetarchive.ipac.caltech.edu/docs/API_mission_stars.html
 * und in den Description-Tags in der VOTable-Datei.
 */
class Hostwerte final {

public:

    /// SIMBAD-Typ (vgl. http://simbad.u-strasbg.fr/simbad/sim-display?data=otypes)
    Wert<std::string> simbad_typ;

    /// Rektaszension [°]. TODO ra ;
    Wert<double> ra;

    /// Deklination [°]. TODO dec ;
    Wert<double> dek;

    /// Eigenbewegung Rektaszension [mas/y].
    Wert<double> eigenbewegung_ra;

    /// Eigenbewegung Deklination [mas/y].
    Wert<double> eigenbewegung_dek;

    /// Eigenbewegung absolut [mas/y].
    double eigenbewegung_abs() const;

    /// Entfernung [ly]. TODO st_dist (planets.csv)
    Wert<double> dist;

    /// Mehrfachstern? TODO (indirekt st_mbolflag)
    Wert<bool> multi;

    /// Spektraltyp [O, B, A, F, G, K, M]. (http://simbad.u-strasbg.fr/guide/chD.htx) TODO st_spttag st_spttype st_lumclass
    Wert<char> spektraltyp;

    /// Zusatz zum Spektraltyp von 0 - 9 zur feineren Klassifizierung.
    Wert<uint16_t> spektraltyp_zusatz;

    /// Enum für die Leuchtkraftklasse: { I, II, III, IV, V }.
    enum Leuchtkraftklasse { I = 1, II = 2, III = 3, IV = 4, V = 5};
    /// Leuchtkraftklasse [Ia, Iab, Ib, II, III, IV, V, sd/VI, D/VII] https://en.wikipedia.org/wiki/Stellar_classification
    Wert<Leuchtkraftklasse> leuchtkraftklasse;

    /// Liefert die Gesamtklassifikation als `std::string`.
    const std::string typ() const;

    /// Effektive Temperatur [K]. TODO st_teff
    Wert<uint32_t> temp;

    /// Radius [Sonnenradien]. TODO st_rad
    Wert<double> r;

    /// Masse [Sonnenmassen]. TODO st_mass ; -
    Wert<double> m;

    /// Alter [Gyr]. TODO st_age ;
    Wert<double> alter;

    /// B-V Farbindex. TODO st_bmv ; FLUX_B - FLUX_V,
    Wert<double> farbindex_b_v;

    /// V-U Farbindex. TODO - ; FLUX_V - FLUX_U
    Wert<double> farbindex_v_u;

    /// Helligkeit [mag] TODO st_vmag st_j2m st_h2m st_ks2m st_lbol [log(solar)] ; FLUX_ B H I J K R U V g i r u z
    Wert<double> hell;

    /* Ideen, Weitere:
     * Radialgeschwindigkeit    RV_VALUE
     * Rotverschiebung          Z_VALUE
     */

    /// Setzt alle von der KI gesetzten Werte zurück.
    void reset();

    /// Liefert serialisierte Hostwerte-Instanz-Daten.
    std::string get_bytes() const;

    /// Deserialisiert alle Werte aus einem `std::string`, der zuvor via `get_bytes()` geschrieben wurde.
    bool deserialisieren(const std::string& bytes);

private:

    /// Enthält generische Zeiger zu allen benutzten Werten.
    const std::vector<Resettable*> ALLE {
        &simbad_typ,
        &ra,
        &eigenbewegung_ra,
        &dek,
        &eigenbewegung_dek,
        &dist,
        &multi,
        &spektraltyp,
        &spektraltyp_zusatz,
        &leuchtkraftklasse,
        &temp,
        &r,
        &m,
        &alter
    };

};
