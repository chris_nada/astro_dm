#pragma once

#include "hostwerte.h"

#include <map>
#include <set>
#include <utility>
#include <vector>
#include <ITexture.h>

class Planet;

/**
 * Datenrepräsentation eines Hostobjektes.
 * Enthält Methoden zum Einlesen von Hosts und Parsen von NASA-CSV + SIMBAD-VOTable-Daten.
 */
class Host final {

public:

    /// Aufzählung der von Exominer berücksichtigten Klassen, denen ein Stern angehören kann.
    enum Klasse {
        UNBEKANNT,
        HYPERRIESE,
        RIESE,
        HAUPTREIHE,
        ROTER_ZWERG,
        BRAUNER_ZWERG,
        WEISSER_ZWERG,
        PULSAR
    };

    /// Konstruktor mit Namen.
    explicit Host(const std::string& name) : name(name) { namen.insert(name); }

    /// Liefert den Host mit gegebenem Key. `nullptr`, wenn nicht vorhanden.
    static Host* get(const std::string& name);

    /// Liefert den unterliegenden Speicher aller Host* (`std::map<std::string, Host*>`).
    static std::map<std::string, Host*> alle() { return hosts; };

    /// Liefert ein Set aller gültiger `Host*`.
    static std::vector<Host*> alle_unique();

    /// Gibt an, ob ein Host mit gegebenem Namen existiert.
    static bool existiert(const std::string& name);

    /// *Ergänzt* die vorhandenen Daten um die in der `DATEI_HOSTS_NASA` enthaltenden Hosts.
    static void aus_csv_ergaenzen();

    /// Liest die VOTables von SIMBAD im Verzeichnis `DIR_HOSTS_SIMBAD` ein.
    static bool einlesen(bool update = false);

    /// Pfad zum Ordner, der SIMBAD-VOTables enthält.
    static constexpr const char* const DIR_HOSTS_SIMBAD = "data/hosts/simbad/";

    /// Pfad zur Datei, in die der Cache geschrieben wird.
    static constexpr const char* const CACHE_DATEI = "data/hosts/cache.dat";

    /// Schreibt alle Hostdaten in eine lokale Cache-Datei zum späteren einlesen (schneller als aus VOTable).
    static bool cache_schreiben();

    /// Liest alle Hostdaten, die in der lokalen Cache-Datei liegen ein. Liefert `true` bei Erfolg.
    static bool cache_lesen();

    /// Fügt einen 'Alternativnamen' dem Host hinzu (falls noch nicht vorhanden).
    inline void add_name(const std::string& name) { if (!name.empty()) namen.insert(name); };

    /// Getter: Name.
    const std::string& get_name() const { return name; }

    /// Getter: Alle vergebenen Namen und Alternativbezeichnungen.
    const std::set<std::string>& get_namen() const { return namen; }

    /// Getter: SIMBAD-OID (Interne von SIMBAD genutzte ID). Leer bedeutet, Quelle des Hosts ist nicht SIMBAD.
    const std::string& get_simbad_oid() const { return simbad_oid; }

    /// Liefert die Werte.
    inline Hostwerte& data() { return daten; }

    /// Liefert die Werte `const`.
    inline const Hostwerte& data() const { return daten; }

    /// Liefert zum Hosts gehörigen Planeten.
    const std::vector<Planet*> get_planeten() const;

    /// Sind genügend Orbitalparameter bekannt, um das System darzustellen?
    bool is_darstellbar() const;

    /// Liefert die Klasse dieses Hosts.
    Host::Klasse get_klasse() const;

    /// Liefert die Klasse dieses Hosts als String.
    std::string get_klasse_als_string() const;

    /// Liefert ein Bild des Hosts aus dem Cache. Falls nicht vorhanden, wird Download versucht. Bei Fehlschlag nullptr.
    irr::video::ITexture* get_image();

private:

    /// Versucht einen Host aus einer VOTable mit gegebenem Pfad auszulesen. Gibt `nullptr`, wenn gescheitert.
    static Host* aus_votable(const std::string& dateiname);

    /// Liefert 'hartkodiert' die Sonne als Host, da sie in vielen Exoplanet-Datenbanken nicht auftaucht.
    static Host* get_sonne();

    /// Entfernt alle Hosts aus dem Speicher.
    static void clear();

    /// Gibt an, wieviele Hosts das Attribut mit gegebenem Key bestimmt haben.
    static unsigned int attribut_anzahl(const std::string& key);

    /// Gibt in der Konsole Informationen zu den Attributen geladener Hosts aus.
    static void debug_attribut_anzahl();

    /// Speicher aller geladener Hosts (Zeiger).
    static inline std::map<std::string, Host*> hosts;

    /// Beschreibungen der Attribute aus den VOTables.
    static inline std::map<std::string, std::string> id_desc;

    /// Parst `rohdaten`, die aus SIMBAD stammen und befüllt `Hostwerte daten`.
    void parse_simbad();

    /**
     * Liefert serialisierte Host-Instanz-Daten, aus denen sich mit dem passenden Konstruktor wieder ein Host aufbauen lässt.
     */
    std::string get_bytes() const;

    /// Name des Hosts.
    std::string name;

    /// Alternativnamen des Hosts.
    std::set<std::string> namen;

    /// Rohdaten des Hosts.
    std::map<std::string, std::string> rohdaten;

    /// SIMBAD OID. Interne von SIMBAD genutzte ID.
    std::string simbad_oid;

    /// Aufbereitete Daten des Hosts.
    Hostwerte daten;

};
