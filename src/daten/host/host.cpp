#include "host.h"
#include "../io/net.h"
#include "../planet/planet.h"
#include "../io/etc.h"
#include "../io/simbad.h"
#include "../konstante.h"
#include "../../ui/ladefenster.h"
#include "../io/sync.h"
#include "../../ui/ui.h"

#include <dlib/misc_api.h>
#include <dlib/dir_nav.h>
#include <sys/stat.h>
#include <dlib/threads.h>

bool Host::einlesen(bool update) {
    std::cout << "Host::einlesen(" << update << ")" << std::endl;
    Ladefenster ladefenster1(L"Hosts werden überprüft...", Planet::alle().size() - 1);
    ladefenster1.update(1);

    // SIMBAD Mirror + Timer wg. Download-Limit (6 Requests pro Sekunde)
    std::array<std::string, 2> SIMBAD_URLS { Simbad::URL1, Simbad::URL2 };
    auto timer = std::time(nullptr);

    // Für jeden Planeten seinen Host laden
    for (unsigned int i = 0; i < Planet::alle().size(); ++i) {
        Planet* planet = Planet::alle()[i];
        if (!planet->get_host_name().empty()) {
            bool download = false;
            // Datei nicht vorhanden => herunterladen.
            if (!update) {
                struct stat buffer;
                download = stat((DIR_HOSTS_SIMBAD + planet->get_host_name()+ ".xml").c_str(), &buffer) != 0;
            }

            // Download -> Simbad konsultieren
            if (update || download) {
                // Simbad erlaubt max. 6 Requests pro Sekunde, wenn Mirror vorhanden - abwechseln
                if (SIMBAD_URLS[1].empty()) {
                    auto warte_ms = 200 - (std::time(nullptr) - timer);
                    if (warte_ms < 1) warte_ms = 1;
                    dlib::sleep(static_cast<unsigned long>(warte_ms));
                }
                else {
                    auto warte_ms = 100 - (std::time(nullptr) - timer);
                    if (warte_ms < 1) warte_ms = 1;
                    dlib::sleep(static_cast<unsigned long>(warte_ms));
                    std::swap(SIMBAD_URLS[0], SIMBAD_URLS[1]);
                }
                std::string url =
                        SIMBAD_URLS[0] +
                        "/sim-id?Ident=" +
                        planet->get_host_name() +
                        "&output.format=VOTable";
                std::replace(url.begin(), url.end(), ' ', '+'); // Leerzeichen zu '+'
                std::string text(Net::download(url));
                if (!text.empty()) {
                    std::ofstream out(DIR_HOSTS_SIMBAD + planet->get_host_name() + ".xml");
                    out << text;
                }
                timer = std::time(nullptr);
            }
            //break; // Debug: Nur für 1 Host runterladen
        }
        if (i % 0b0010'0000 == 0 /* 64 */) ladefenster1.update(i);
    }

    // 'Alte' Sterne löschen
    clear();

    // SIMBAD - Dateien auslesen
    dlib::directory hosts_dir(DIR_HOSTS_SIMBAD);
    std::vector<std::string> hosts_dateien;
    for (const auto& f : hosts_dir.get_files()) hosts_dateien.push_back(f.name());
    Ladefenster ladefenster2(L"VOTables werden eingelesen...", hosts_dateien.size() - 1);

    std::vector<Host*> hosts_temp(hosts_dateien.size(), nullptr);

    //dlib::parallel_for(4, 0, (long) hosts_dateien.size(), [&](long i) {
    for (unsigned int i = 0; i < hosts_dateien.size(); ++i) {
        const std::string& f = hosts_dateien[i];
        Host* hosts_neu = aus_votable(DIR_HOSTS_SIMBAD + f);
        hosts_temp[i] = hosts_neu;
        if (i % 10 == 0) ladefenster2.update(i);
    } //);
    for (Host* host : hosts_temp) if (host) hosts[host->get_name()] = host;

    // Sonne hinzufügen
    Host* sonne = get_sonne();
    if (sonne) hosts[sonne->get_name()] = sonne;

    // Daten ergänzen
    aus_csv_ergaenzen();

    // Debug
    std::cout << "\t" << hosts.size() << " Hostobjekte geladen." << std::endl;
    //debug_attribut_anzahl();
    return true;
}

Host* Host::aus_votable(const std::string& dateiname) {
    std::string header("Host::aus_votable(" + dateiname + "): ");
    Host* host_neu = nullptr; // neuer Host
    bool fehler    = false;   // VOTable Errortext vorhanden? Oder auch sonstiger Fehler.
    if (dateiname.size() > 4) {
        std::ifstream in(dateiname);
        if (in.good()) {

            // Datei auslesen
            std::vector<std::string> werte; // VOTable Werte aus <TR>...</TR>
            std::stringstream ss;
            ss << in.rdbuf();

            // Zeilen parsen
            std::vector<std::string> fields;
            std::decay<decltype(std::string::npos)>::type find;
            for (std::string zeile; std::getline(ss, zeile);) {

                // FIELD?
                find = zeile.find("<FIELD ID=");
                if (find != std::string::npos) {
                    std::string temp(zeile.substr(find+11));
                    temp = temp.substr(0, temp.find('"'));
                    fields.push_back(temp);
                    continue;
                }

                // DESCRIPTION?
                find = zeile.find("<DESCRIPTION>");
                if (!fields.empty() && find != std::string::npos) {
                    if (id_desc.count(fields.back()) != 0) {
                        id_desc[fields.back()] = zeile.substr(find + 13, zeile.find("</") - 13);
                        continue; // TODO nicht in Benutzung
                    }
                }

                // ERROR?
                find = zeile.find("<INFO name=\"Error\"");
                if (find != std::string::npos) {
                    //std::cerr << "Warnung: " << header << zeile << "\n";
                    fehler = true;
                    break;
                }

                // TABLEDATA?
                find = zeile.find("<TR>");
                if (find != std::string::npos) {
                    Etc::replace_all(zeile, "<TR>",  "");
                    Etc::replace_all(zeile, "<TD>",  "");
                    Etc::replace_all(zeile, "</TD>", "~"); // weil nie verwendet
                    Etc::replace_all(zeile, "</TR>", "");
                    werte = Etc::tokenize(zeile, '~');
                    werte.pop_back(); // Letztes Item ist Müll

                    // Ausversehen Planet? Manchmal gibt SIMBAD statt des Sterns eine Planeten-VOTable.
                    for (const std::string& wert : werte) if (wert == "Planet" || wert == "Planet?" || wert == "Inexistent") {
                        //std::cerr << "Warnung: " << header << "Host ist " << wert << ". Wird uebersprungen." << "\n";
                        fehler = true;
                        break;
                    }
                }
            }
            // Mögliche Probleme feststellen
            //if (!fields.empty() && fields.size() != 373) std::cout << header << fields.size() << " Felder gefunden. (Standard=373)\n";
            if (fields.empty() && !fehler)               std::cerr << header << "Keine Felder gefunden. Unbekannter Fehler." << std::endl;
            if (werte.size() != fields.size())           std::cerr << header << werte.size() << '/' << fields.size() << " Werte/Felder ungleich." << std::endl;

            // Kein Fehler
            if (!fields.empty() && !fehler) {

                // Anzahl Werte == Felder -> OK
                dlib::file datei(dateiname);
                host_neu = new Host(datei.name().substr(0, datei.name().size()-4));

                // Werte eintragen
                for (uint16_t i = 0; i < werte.size(); ++i) {
                    host_neu->rohdaten[fields[i]] = werte[i];
                }
                host_neu->parse_simbad();
            }
        } else std::cerr << header << "Datei nicht lesbar." << std::endl;
    } else std::cerr << header << "Dateiname ungueltig." << std::endl;
    return host_neu;
}

void Host::debug_attribut_anzahl() {
    std::cout << "Host::debug_attribut_anzahl(); hosts.size() = " << hosts.size() << std::endl;
    // Alle Werte durchgehen
    for (const auto& id : id_desc) std::cout << '\t' << id.first << ": " << attribut_anzahl(id.first) << '\n';
}

unsigned int Host::attribut_anzahl(const std::string& key) {
    unsigned int vorhanden = 0;
    for (const auto& paar : hosts) {
        // Zählen wie oft das Attribut einen Eintrag hat (nicht leer ist)
        try { if (!paar.second->rohdaten.at(key).empty()) vorhanden++; }
        catch (std::exception& e) { /* Attribut nicht vorhanden. */ }
    }
    return vorhanden;
}

std::vector<Host*> Host::alle_unique() {
    std::set<Host*> hosts_set;

    // Wenn leer: Sonnensystem laden
    if (hosts.empty() && Planet::alle().empty()) {
        Host* sonne = get_sonne();
        hosts[sonne->get_name()] = sonne;
        Planet::sonnensystem_laden();
    }

    for (const auto& h : hosts) if (h.second) hosts_set.insert(h.second);
    std::vector<Host*> hosts_unique(hosts_set.begin(), hosts_set.end());
    return hosts_unique;
}

void Host::parse_simbad() {
    //std::cout << "Host::parse_simbad() / " << name << '\n'; // DEBUG
    {   // Objekttyp
        const std::string temp(rohdaten["OTYPE_S"]);
        daten.simbad_typ.set_wert_data(rohdaten["OTYPE_S"]);
        if (Simbad::OTYPE.count(daten.simbad_typ()) == 1) daten.simbad_typ.set_beschreibung(Simbad::OTYPE.at(daten.simbad_typ()));
        //std::cout << "OTYPE_S=" << daten.simbad_typ.get_beschreibung() << '\n'; // DEBUG

        // Simbad-interne ID
        simbad_oid = rohdaten["OID4"];
        if (!simbad_oid.empty()) simbad_oid.erase(0, 1); // @ entfernen
    }
    {   // Spektraltyp / Leuchtkraft
        const std::string temp(rohdaten["SP_TYPE"]);
        // Mindestens Spektraltyp + Leuchtkraftklasse vorhanden?
        if (!temp.empty()) {
            //std::cout << temp << '\n'; // DEBUG
            daten.spektraltyp.set_beschreibung(temp); // Originalangabe

            // Spektraltyp bestimmen
            static constexpr const char* const valide_typen = "OBAFGKM";
            if (std::strchr(valide_typen, temp[0])) daten.spektraltyp.set_wert_data(temp[0]);

            // Spektraltypzusatz vorhanden?
            if (temp.size() >= 2) {
                static constexpr const char* const valide_zusaetze = "0123456789";
                if (std::strchr(valide_zusaetze, temp[1])) {
                    daten.spektraltyp_zusatz.set_wert_data((uint16_t) (temp[1] - '0'));
                }
                // Leuchtkraftklasse // TODO Bug: wird nicht korrekt eingelesen
                static const constexpr auto n = std::string::npos;
                if      (temp.find("IV")!=n)  daten.leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::IV);
                else if (temp.find('V')!=n)   daten.leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::V);
                else if (temp.find("III")!=n) daten.leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::III);
                else if (temp.find("II")!=n)  daten.leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::II);
                else if (temp.find('I')!=n)   daten.leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::I);
            }
        }
    }
    {   // Radius
        if (!rohdaten["diameter_diameter"].empty() && dlib::tolower(rohdaten["diameter_unit"]) == "km") {
            try {
                std::string lower(dlib::tolower(rohdaten["diameter_diameter"]));
                double d = (std::stod(lower) * 0.5) / Konstante::R_SONNE; // -> Radius, -> Sonnenradien
                daten.r.set_wert_data(d);
            } catch (std::exception& e) { /* Macht nix. */ }
        }
    }
    {   // Entfernung
        if (!rohdaten["distance_distance"].empty() && !rohdaten["distance_unit"].empty()) {
            try {
                double d = std::stod(rohdaten["distance_distance"]);
                if      (rohdaten["distance_unit"] == "kpc") d *= 1'000;
                else if (rohdaten["distance_unit"] == "Mpc") d *= 1'000'000;
                else if (rohdaten["distance_unit"] == "mpc") d *= 1'000'000;
                daten.dist.set_wert_data(Konstante::LY_PC * d); // pc -> ly
            } catch (std::exception& e) { /* Macht nix. */ }
        }
    }
    // 'Einfach' auszulesende Daten
    daten.ra.  set_wert_aus_datastring(rohdaten["RA_d"]);               // Rektaszension,
    daten.dek. set_wert_aus_datastring(rohdaten["DEC_d"]);              // Deklination
    daten.eigenbewegung_ra. set_wert_aus_datastring(rohdaten["PMRA"]);  // Eigenbewegung Rektaszension.
    daten.eigenbewegung_dek.set_wert_aus_datastring(rohdaten["PMDEC"]); // Eigenbewegung Deklination.
    daten.temp.set_wert_aus_datastring(rohdaten["Fe_H_Teff"]);          // Effektive Temperatur.

    // TODO FLUX_
    // TODO abs. Helligkeit / bol. Helligkeit
    // TODO Farbindizes (?)
}

const std::vector<Planet*> Host::get_planeten() const {
    std::vector<Planet*> planeten;
    for (auto planet : Planet::alle()) {
        for (const auto& alias : namen) {
            if (planet->get_host_name() == alias) {
                planeten.push_back(planet);
                break;
            }
        }
    }
    return planeten;
}

bool Host::is_darstellbar() const {
    for (Planet* planet : get_planeten()) {
        if (!planet->data().orbit_a.is_bekannt()) return false;
    }
    return true;
}

Host* Host::get_sonne() {
    Host* sonne = new Host("Sonne");
    sonne->namen.insert("Sol");
    sonne->namen.insert("Sun");
    sonne->data().multi.set_wert_data(false);
    sonne->data().spektraltyp.set_beschreibung("G2V");
    sonne->data().spektraltyp.set_wert_data('G');
    sonne->data().spektraltyp_zusatz.set_wert_data(2);
    sonne->data().leuchtkraftklasse.set_wert_data(Hostwerte::Leuchtkraftklasse::V);
    sonne->data().temp.set_wert_data(5778);
    sonne->data().r.set_wert_data(1.0);
    sonne->data().m.set_wert_data(1.0);
    sonne->data().m.set_wert_data(1.0);
    sonne->data().alter.set_wert_data(4.6);
    return sonne;
}

bool Host::existiert(const std::string& name) {
    return get(name) != nullptr;
}

Host* Host::get(const std::string& name) {
    // 1. Schnelle Suche
    try { return hosts.at(name); }
    catch (std::exception& e) {
        // 2. Langsame Suche / Prüft auch Alternativnamen case-insensitive
        for (const auto& host : hosts) {
            for (const auto& host_name : host.second->namen) {
                if (dlib::strings_equal_ignore_case(name, host_name)) return host.second;
            }
        }
    }
    return nullptr;
}

void Host::aus_csv_ergaenzen() {
    if (std::ifstream in(Sync::DATEI_HOSTS); in.good()) {
        std::cout << "Host::" << __func__ << ' ' << Sync::DATEI_HOSTS << '\n';
        unsigned int zaehler = 0;

        // Header auslesen
        std::string zeile;
        std::getline(in, zeile);
        auto tokens = Etc::tokenize(zeile, ',');
        std::map<std::string, unsigned int> ids;
        for (unsigned int i = 0; i < tokens.size(); ++i) ids[tokens[i]] = i;

        // Daten Zeile für Zeile lesen + mit bestehenden Hosts abgleichen
        for (;std::getline(in, zeile);) {
            tokens = Etc::tokenize(zeile, ',');
            if (tokens.size() == ids.size()) {
                std::array<std::string, 5> nasa_namen;
                nasa_namen[0] = tokens[ids["hip_name"]];
                nasa_namen[1] = tokens[ids["hd_name"]];
                nasa_namen[2] = tokens[ids["gj_name"]];
                nasa_namen[3] = tokens[ids["tm_name"]];
                nasa_namen[4] = tokens[ids["star_name"]];

                // Für jeden Alternativnamen versuchen, Host zu finden
                for (const auto& alternativname : nasa_namen) {
                    Host* temp_host = get(alternativname);
                    if (temp_host) { // gefunden
                        for (const auto& n : nasa_namen) temp_host->add_name(n);
                        ++zaehler;
                        break;
                    }
                }
            }
        }
        std::cout << zaehler << " Hosts aus " << Sync::DATEI_HOSTS << " ergaenzt.\n";
    } else std::cerr << Sync::DATEI_HOSTS << " nicht lesbar.\n";
}

Host::Klasse Host::get_klasse() const {
    if (daten.leuchtkraftklasse.is_bekannt()) {
        switch (daten.leuchtkraftklasse()) {
            case Hostwerte::Leuchtkraftklasse::I:   return Klasse::HYPERRIESE;
            case Hostwerte::Leuchtkraftklasse::II:  return Klasse::RIESE;
            case Hostwerte::Leuchtkraftklasse::III: return Klasse::RIESE;
            case Hostwerte::Leuchtkraftklasse::IV:  return Klasse::RIESE;
            case Hostwerte::Leuchtkraftklasse::V: {
                if (daten.spektraltyp() == 'D') return Klasse::WEISSER_ZWERG;
                std::string spektraltyp_klein = dlib::tolower(daten.spektraltyp.get_beschreibung());
                if (spektraltyp_klein.find("sd") != std::string::npos) return Klasse::BRAUNER_ZWERG;
                return Klasse::HAUPTREIHE;
                break;
            }
            default: break;
        }
    }
    return Klasse::UNBEKANNT;
}

std::string Host::get_klasse_als_string() const {
    switch (get_klasse()) {
        case HYPERRIESE:    return "Hyperriese";
        case RIESE:         return "Riese";
        case HAUPTREIHE:    return "Hauptreihe";
        case ROTER_ZWERG:   return "Roter Zwerg";
        case BRAUNER_ZWERG: return "Brauner Zwerg";
        case WEISSER_ZWERG: return "Weißer Zwerg";
        case PULSAR:        return "Pulsar";
        case UNBEKANNT:     return "Unbekannt";
        default:            return "Unbekannt";
    }
}

std::string Host::get_bytes() const {
    std::stringstream ss;
    try {
        dlib::serialize(name, ss);
        dlib::serialize(namen, ss);
        dlib::serialize(rohdaten, ss);
        dlib::serialize(simbad_oid, ss);
        ss << daten.get_bytes();
    } catch (std::exception& e) {
        std::cerr << "Host::get_bytes() Host=" << name << ": " << e.what() << std::endl;
    }
    return ss.str();
}

bool Host::cache_schreiben() {
    try {
        std::ofstream out(CACHE_DATEI);
        std::vector<std::string> cache_daten(hosts.size());
        for (const auto& paar : hosts) { cache_daten.push_back(paar.second->get_bytes()); }
        dlib::serialize(cache_daten, out);
        return true;
    } catch (std::exception& e) {
        std::cerr << "Host::" << __func__ << " Fehler: ";
        std::cerr << e.what() << std::endl;
        return false;
    }
}

bool Host::cache_lesen() {
    try {
        // Deserialisieren aus Datei
        std::ifstream in(CACHE_DATEI);
        std::vector<std::string> cache_daten;
        dlib::deserialize(cache_daten, in);

        // Bestehende Hosts löschen
        clear();

        // Einzelne Hosts deserialisieren
        for (const auto& bytes : cache_daten) {
            std::stringstream ss(bytes);

            // Temporäre Werte zum Host-Erstellen
            decltype(name) temp_name;
            decltype(namen) temp_namen;
            decltype(rohdaten) temp_rohdaten;
            decltype(simbad_oid) temp_simbad_oid;
            std::string temp_hostdaten;

            // Einzelne Werte deserialisieren
            dlib::deserialize(temp_name, ss);
            dlib::deserialize(temp_namen, ss);
            dlib::deserialize(temp_rohdaten, ss);
            dlib::deserialize(temp_simbad_oid, ss);
            dlib::deserialize(temp_hostdaten, ss);

            // Host erstellen
            Host* host       = new Host(temp_name);
            hosts[temp_name] = host;
            host->namen      = temp_namen;
            host->rohdaten   = temp_rohdaten;
            host->simbad_oid = temp_simbad_oid;
            host->daten.deserialisieren(temp_hostdaten);
        }
        return true;
    } catch (std::exception& e) {
        std::cerr << "Host::" << __func__ << " Fehler: ";
        std::cerr << e.what() << std::endl;
        return false;
    }
}

void Host::clear() {
    for (auto paar : hosts) { delete paar.second; paar.second = nullptr; }
    hosts.clear();
}

irr::video::ITexture* Host::get_image() {
    std::string thumb_pfad = "data/gfx/thumbs/" + get_simbad_oid() + ".jpeg";
    {   // Download?
        if (std::ifstream datei_check(thumb_pfad); !datei_check.good()) {
            std::string image_url =
                    Simbad::URL_THUMBS +
                    get_simbad_oid() + "&size=300&legend=true";
            Net::download(image_url, thumb_pfad);
        }
    }
    // Bild -> Textur
    irr::video::ITexture* image = UI::driver->getTexture(thumb_pfad.c_str());
    return image;
}
