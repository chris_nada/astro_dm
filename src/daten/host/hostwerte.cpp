#include "hostwerte.h"

double Hostwerte::eigenbewegung_abs() const {
    if (!(eigenbewegung_ra && eigenbewegung_dek)) return 0;
    double temp = std::sqrt(std::pow(eigenbewegung_ra(), 2) + std::pow(eigenbewegung_dek(), 2));
    return temp;
}

void Hostwerte::reset() {
    for (Resettable* wert : ALLE) wert->reset();
}

std::string Hostwerte::get_bytes() const {
    std::stringstream ss;
    for (const Resettable* const wert : ALLE) dlib::serialize(wert->get_bytes(), ss);
    return ss.str();
}

bool Hostwerte::deserialisieren(const std::string& bytes) {
    try {
        std::stringstream ss(bytes);

        auto deserialisieren = [&](auto& wert) {
            std::string temp;
            dlib::deserialize(temp, ss);
            (void) wert; // TODO
        };

        deserialisieren(simbad_typ);
        deserialisieren(ra);
        deserialisieren(eigenbewegung_ra);
        deserialisieren(dek);
        deserialisieren(eigenbewegung_dek);
        deserialisieren(dist);
        deserialisieren(multi);
        deserialisieren(spektraltyp);
        deserialisieren(spektraltyp_zusatz);
        deserialisieren(leuchtkraftklasse);
        deserialisieren(temp);
        deserialisieren(r);
        deserialisieren(m);
        deserialisieren(alter);
        return true;
    } catch (std::exception& e) {
        std::cerr << "Hostwerte deserialisieren fehlgeschlagen: " << e.what() << std::endl;
        return false;
    }
}

const std::string Hostwerte::typ() const {
    std::stringstream ss;
    ss << spektraltyp;
    ss << spektraltyp_zusatz;
    if (leuchtkraftklasse) switch (Leuchtkraftklasse()) {
            case I:   ss << 'I';   break;
            case II:  ss << "II";  break;
            case III: ss << "III"; break;
            case IV:  ss << "IV";  break;
            case V:   ss << 'V';   break;
            default: break;
        }
    return ss.str();
}
