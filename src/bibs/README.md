In diesem Verzeichnis befinden sich Programmbibliotheken in Quelltextform,
die meist aus Gründen der Kompatibilität in die Quelltext-/Projektstruktur integriert wurden.
Alle Dateien in den Unterverzeichnissen wurden nicht von den Autoren der Hauptsoftware verfasst.
Textdateien mit Lizenzen finden sich in den jeweiligen Ordnern der Programmbibliotheken.