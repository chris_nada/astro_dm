#pragma once

#include "ui.h"
#include <iostream>

/// Basisklasse, von der geerbt wird zur Erzeugung neuer Fenster.
class Fenster : public irr::IEventReceiver {

public:

    /// Konstruktor, der den Irrlicht-EventReceiver auf diese Instanz setzt.
    Fenster(IEventReceiver* parent = nullptr) : parent(parent) {
        std::cout << "Fenster()" << std::endl;
        UI::device->setEventReceiver(this);
    };

    /// Enthält eine Schleife, die alle Events in der GUI handelt.
    virtual void zeigen() = 0;

    /// Destruktor, der den Irrlicht-EventReceiver auf `nullptr` setzt.
    ~Fenster() {
        std::cout << "~Fenster()" << std::endl;
        UI::device->setEventReceiver(parent);
    };

protected:

    /// Funktion baut alle GUI-Elemente auf.
    virtual bool aufbauen() = 0;

    /// Bei schließen des Fensters wird der EventReceiver auf diesen zurückgesetzt.
    IEventReceiver* parent;

};
