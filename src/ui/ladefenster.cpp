#include "ladefenster.h"
#include "ui.h"

#include <iostream>
#include <mutex>
#include <dlib/threads/posix.h>

Ladefenster::Ladefenster(const irr::core::stringw& titel, unsigned int total) : total(total > 0 ? total : 1) {
    // Fenster erzeugen
    fenster = UI::guie->addWindow({
          static_cast<int>(UI::driver->getScreenSize().Width/2  - 160),
          static_cast<int>(UI::driver->getScreenSize().Height/2 -  24 + (50 * anzahl)),
          static_cast<int>(UI::driver->getScreenSize().Width/2  + 160),
          static_cast<int>(UI::driver->getScreenSize().Height/2 +  24 + (50 * anzahl))
          }, true, titel.c_str()
    );
    ++anzahl;

    // Text erzeugen
    text = UI::guie->addStaticText(L"Fortschritt: 0%", {
          fenster->getClientRect().getCenter().X -  76,
          fenster->getClientRect().getCenter().Y -   8,
          fenster->getClientRect().getCenter().X + 100,
          fenster->getClientRect().getCenter().Y +  24
          }, false, true, fenster
    );
    update(0);
}

Ladefenster::~Ladefenster() {
    fenster->remove();
    --anzahl;
}

void Ladefenster::update(unsigned int fortschritt) {
    dlib::mutex mutex;
    dlib::auto_mutex lock(mutex);
    jetzt = fortschritt;

    // GUI Update
    irr::core::stringw text_str = L"Fortschritt: ";
    text_str += std::to_wstring((int) (100.f * jetzt / total)).c_str();
    text_str += L"%";
    text->setText(text_str.c_str());
    UI::driver->beginScene(true, true, UI::FARBE_HG);
    UI::guie->drawAll();
    UI::driver->endScene();
}

void Ladefenster::update() {
    ++jetzt;
    update(jetzt);
}

