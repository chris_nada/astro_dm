#include <irrlicht.h>
#include "../../daten/planet/planet.h"
#include "../../daten/host/host.h"
#include "../ui.h"

using namespace irr;

/**
 * Von Irrlichts `IEventReceiver` erbende Klasse (da eigenes Fenster)
 * zur Darstellung eines Planetensystems in 3D.
 */
class Systemview final : public IEventReceiver {

public:

    /**
     * Ein von extern erzeugter Konstruktoraufruf auf diesen ctor reicht aus, um die Simulation zu starten.
     * @param hosts Zu simulierende Hosts.
     * @param planeten Zu simulierende Planeten.
     */
    Systemview(const std::vector<Host*>& hosts, const std::vector<Planet*>& planeten);

    /// Verwaltungsfunktion von Nutzereingaben, die während der Simulation empfangen werden.
    bool OnEvent(const SEvent& event) override;

    /// Destruktor muss aufgerufen werden für korrekte Speicherdeallokation.
    virtual ~Systemview();

private:

    /// Leerer Konstruktor verboten.
    Systemview() = default;;

    /// Startet die Simulation. Liefert eine Zahl ungleich 0 bei Problemen.
    int start();

    /// Hilfsmethode, die die *Umgebung*, also den 3D Raum, erzeugt. Liefert `false` bei Problemen.
    bool erstelle_raum();

    /// Hilfsmethode, die die Hosts erzeugt. Liefert `false` bei Problemen.
    bool erstelle_host();

    /// Hilfsmethode, die die Planeten erzeugt. Liefert `false` bei Problemen.
    bool erstelle_planeten();

    /// Hilfsmethode, die die Nutzeroberfläche erzeugt. Liefert `false` bei Problemen.
    bool erstelle_ui();

    /// Schaltet den Renderer um.
    void set_material();

    /// Setzt die Kamera zurück auf die Anfangsposition.
    void reset_kamera();

    /// Führt einen sogenannten 'Tick' in der Simulation aus.
    void update();

    /* UI */

    gui::IGUIEnvironment* guie;
    gui::IGUIWindow*      fenster_renderer;
    gui::IGUIWindow*      fenster_planetinfo;
    gui::IGUIListBox*        gui_list_box;
    gui::IGUIStaticText*     problem_text;
    scene::ISceneManager*    scenectrl;

    /* 3D Objekte */

    scene::ICameraSceneNode* camera;
    int kamerafokus = 0;
    scene::ISceneNode*       raum_node;
    scene::ILightSceneNode* host_light_node;
    std::vector<scene::ISceneNode*> node_planeten;

    /* Modell */

    std::vector<Host*>   hosts;
    std::vector<Planet*> planeten;

    /* Engine */

    bool loop;
    IrrlichtDevice* device;
    video::IVideoDriver* video_driver;
    video::IMaterialRenderer* renderer;

};
