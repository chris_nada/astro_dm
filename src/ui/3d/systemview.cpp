#include "systemview.h"
#include "../ui.h"
#include "../../daten/io/etc.h"

#include <iostream>

Systemview::Systemview(const std::vector<Host*>& hosts, const std::vector<Planet*>& planeten) :
        hosts(hosts), planeten(planeten), loop(true) {
    std::cout << "Systemview()" << std::endl;
    // Benötigte Systempointer
    device = UI::device;
    video_driver = device->getVideoDriver();
    video_driver->setTextureCreationFlag(video::ETCF_ALWAYS_32_BIT, true);
    scenectrl = device->getSceneManager();
    guie = UI::guie;

    // Kamera einstellen
    reset_kamera();
    std::cout << "Renderdistanz = " << camera->getFarValue() << '\n';

    // Mauszeiger verstecken
    device->getCursorControl()->setVisible(false);

    // Raum
    //video_driver->setFog(video::SColor(0,138,125,81), video::EFT_FOG_LINEAR, 250, 1000, .003f, true, false);
    erstelle_raum();

    // Host erstellen
    erstelle_host();

    // Planeten erstellen
    erstelle_planeten();

    // UI erstellen
    erstelle_ui();

    start();
}

Systemview::~Systemview() {
    std::cout << "~Systemview()" << std::endl;
    scenectrl->clear();
    UI::guie->clear();
}

int Systemview::start() {
    std::cout << "Systemview::start()" << std::endl;
    set_material();
    device->setEventReceiver(this);

    while(loop && device->run() && device->isWindowActive()) {
        update();
        video_driver->beginScene(true, true, 0);
        scenectrl->drawAll();
        guie->drawAll();
        video_driver->endScene();
    }
    return 0;
}

bool Systemview::OnEvent(const SEvent& event) {
    // Taste (Tastatur) gedrückt
    if (event.EventType == irr::EET_KEY_INPUT_EVENT && event.KeyInput.PressedDown) {

        switch (event.KeyInput.Key) {
            case irr::KEY_KEY_0: kamerafokus = 0; return true;
            case irr::KEY_KEY_1: kamerafokus = 1; return true;
            case irr::KEY_KEY_2: kamerafokus = 2; return true;
            case irr::KEY_KEY_3: kamerafokus = 3; return true;
            case irr::KEY_KEY_4: kamerafokus = 4; return true;
            case irr::KEY_KEY_5: kamerafokus = 5; return true;
            case irr::KEY_KEY_6: kamerafokus = 6; return true;
            case irr::KEY_KEY_7: kamerafokus = 7; return true;
            case irr::KEY_KEY_8: kamerafokus = 8; return true;
            case irr::KEY_KEY_9: kamerafokus = 9; return true;
            case irr::KEY_ESCAPE: loop = false; return true;
            default: break;
        }

        // E / R wechselt Renderer
        if (gui_list_box) {
            // Rendererliste setzen
            int sel = gui_list_box->getSelected();
            if (event.KeyInput.Key == irr::KEY_KEY_R) ++sel;
            else if (event.KeyInput.Key == irr::KEY_KEY_E) --sel;
            if (sel > 2) sel = 0;
            else if (sel < 0) sel = 2;
            if (sel != gui_list_box->getSelected()) {
                gui_list_box->setSelected(sel);
                set_material();
            }
        }
    }
    return false;
}

void Systemview::set_material() {
    std::cout << "Systemview::setMaterial()" << std::endl;
    video::E_MATERIAL_TYPE type = video::EMT_SOLID;

    switch(gui_list_box->getSelected()) {
        case 0: type = video::EMT_SOLID; break;
        case 1: type = video::EMT_NORMAL_MAP_SOLID; break;
        case 2: type = video::EMT_PARALLAX_MAP_SOLID; break;
    }
    if (raum_node) raum_node->setMaterialType(type);

    switch(gui_list_box->getSelected()) {
        case 0: type = video::EMT_TRANSPARENT_VERTEX_ALPHA; break;
        case 1: type = video::EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA; break;
        case 2: type = video::EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA; break;
    }
    for (irr::scene::ISceneNode* p : node_planeten) if (p) p->setMaterialType(type);

    renderer = video_driver->getMaterialRenderer(type);

    // Fehlermeldungen anzeigen
    problem_text->setVisible(!renderer || renderer->getRenderCapability() != 0);
}

bool Systemview::erstelle_raum() {
    std::cout << "Systemview::erstelle_raum()" << std::endl;
    raum_node = scenectrl->addSkyDomeSceneNode(
            video_driver->getTexture("data/gfx/skymap.jpg"), // Textur
            16, // horizontale Auflösung
            8, // vertikale Auflösung
            1.0f, // Ausdehnung
            2.0f, // 1.0 = halbsphärisch, 2.0 = sphärisch
            camera->getFarValue() // Reichweite
    );
    return (bool) raum_node;
}

bool Systemview::erstelle_host() {
    std::cout << "Systemview::erstelle_host()" << std::endl;
    float scale = 5.0f; // Skalierungsfaktor

    // Leuchten
    host_light_node = scenectrl->addLightSceneNode(nullptr,
         core::vector3df(0, 0, 0), // Position
         video::SColorf(1.0f, 0.2f, 0.2f, 0.0f), // Lichtfarbe
         8000.0f * scale); // Radius

    host_light_node->enableCastShadow(true);
    host_light_node->setLightType(irr::video::ELT_POINT);
    host_light_node->setRadius(0x00'FF'FF'FFf);

    // Billboard-Effekt hinzufügen
    scene::IBillboardSceneNode* bill = scenectrl->addBillboardSceneNode(host_light_node,
                                                                        core::dimension2d<f32>(120 * scale, 120 * scale));
    bill->setMaterialFlag(video::EMF_LIGHTING, false);
    bill->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    bill->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);
    bill->setMaterialTexture(0, video_driver->getTexture("data/gfx/particlered.bmp"));

    // Partikelemitter
    scene::IParticleSystemSceneNode* ps = scenectrl->addParticleSystemSceneNode(false, host_light_node);
    scene::IParticleEmitter* em = ps->createBoxEmitter(
            core::aabbox3d<f32>(-3, 0, -3, 3, 1, 3),
            core::vector3df(0.0f, 0.0f, 0.0f),
            80,100,
            video::SColor(10,255,255,255), video::SColor(10,255,255,255),
            400,1100);
    em->setMinStartSize(core::dimension2d<f32>(30.0f * scale, 30.0f * scale));
    em->setMaxStartSize(core::dimension2d<f32>(50.0f * scale, 50.0f * scale));

    ps->setEmitter(em);
    em->drop();

    // Partikelaffektor
    scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();
    ps->addAffector(paf);
    paf->drop();

    // Materialeinstellungen
    ps->setMaterialFlag(video::EMF_LIGHTING, true);
    ps->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps->setMaterialTexture(0, video_driver->getTexture("data/gfx/fireball.bmp"));
    ps->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);
    return true;
}

bool Systemview::erstelle_planeten() {
    std::cout << "Systemview::erstelle_planeten()" << std::endl;
    for (Planet* planet : planeten) {
        scene::IAnimatedMesh* earthMesh = scenectrl->getMesh("data/gfx/planet/earth.x");
        if (earthMesh) {
            // Meshmanipulator erzeugen
            scene::IMeshManipulator* manipulator = scenectrl->getMeshManipulator();

            // Tangentenmesh erzeugen
            scene::IMesh* tangentSphereMesh = manipulator->createMeshWithTangents(earthMesh->getMesh(0));

            // Größe einstellen
            {
                core::CMatrix4<f32> m;
                float radius = 5.f; // Radius Erde
                if (planet->data().r.is_bekannt()) radius *= planet->data().r();
                m.setScale(core::vector3df(radius, radius, radius));
                manipulator->transform(tangentSphereMesh, m);
            }
            irr::scene::ISceneNode* node_planet = scenectrl->addMeshSceneNode(tangentSphereMesh);

            // Position
            node_planet->setPosition(core::vector3df(-70, 130, 45));

            // Text
            const wchar_t letter = planet->data().letter();
            /* irr::scene::IBillboardTextSceneNode* label = */ scenectrl->addBillboardTextSceneNode(
                    UI::skin->getFont(),
                    &letter,
                    node_planet,
                    {10.f, 10.f},
                    {0.f, 30.f, 0.f}
            );

            // Zufällige Textur
            std::string texturpfad("data/gfx/planet/solid/textur" + std::to_string(
                    Etc::zufall<uint16_t>(1, 9)) + ".jpg"
            );
            irr::video::ITexture* planet_tex = video_driver->getTexture(texturpfad.c_str());
            if (planet_tex) {
                node_planet->setMaterialTexture(0, planet_tex);
                //node_planet->setMaterialType(irr::video::EMT_ONETEXTURE_BLEND);
            }
            else std::cerr << "Textur " << texturpfad << " konnte nicht geladen werden.\n";

            // Textur / Bumpmap
            /*video::ITexture* earthNormalMap = video_driver->getTexture("data/gfx/earthbump.jpg");
            if (earthNormalMap) {
                video_driver->makeNormalMapTexture(earthNormalMap, 20.0f);
                node_planet->setMaterialTexture(1, earthNormalMap);
                node_planet->setMaterialType(video::EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA);
            }*/

            // Nebeleffekt
            //node_planet->setMaterialFlag(video::EMF_FOG_ENABLE, true);

            // Rotation
            scene::ISceneNodeAnimator* anim_rotation =
                    scenectrl->createRotationAnimator(core::vector3df(0, 0.2f, 0));
            node_planet->addAnimator(anim_rotation);
            anim_rotation->drop();

            // Bahnelemente / Orbitalparameter
            auto orbit_a = static_cast<irr::f32>(5000.f * planet->data().orbit_a());
            std::cout << "\n\tGrosse Halbachse " << planet->data().letter << " = " << orbit_a << '\n';

            float orbit_p = 1.f;
            if (planet->data().orbit_p.is_bekannt()) orbit_p = (float) planet->data().orbit_p();
            std::cout << "\tOrbitale Periode " << planet->data().letter << " = " << orbit_p << '\n';

            float orbit_t = 0.f;
            if (planet->data().orbit_t.is_bekannt()) orbit_t = (float) planet->data().orbit_t();
            std::cout << "\tPeriapsiszeit " << planet->data().letter << " = " << orbit_t << '\n';

            float orbit_e = 0.f;
            if (planet->data().orbit_e.is_bekannt()) orbit_e = (float) planet->data().orbit_e();
            std::cout << "\tExzentrizitaet " << planet->data().letter << " = " << orbit_e << "\n\n" << std::endl;

            // Orbit erstellen
            scene::ISceneNodeAnimator* anim_orbit = scenectrl->createFlyCircleAnimator(host_light_node->getPosition(),
                orbit_a, // Radius
                0.005f / orbit_p, // Geschwindigkeit
                {0.0f, 1.0f, 0.0f}, // Richtung
                orbit_t, // Startposition
                0.0f * orbit_e // Ellipsoid
            );
            node_planet->addAnimator(anim_orbit);
            anim_orbit->drop();

            // Mesh-Destruktor
            tangentSphereMesh->drop();
            node_planeten.push_back(node_planet);
        }
    }
    return true;
}

bool Systemview::erstelle_ui() {
    std::cout << "Systemview::erstelle_ui()" << std::endl;

    // Renderwahlfenster + Liste
    fenster_renderer = guie->addWindow(core::rect<s32>(
            video_driver->getScreenSize().Width - 200, video_driver->getScreenSize().Height - 100,
            video_driver->getScreenSize().Width,       video_driver->getScreenSize().Height),
            false, L"Renderer Wechseln: E / R");
    gui_list_box = guie->addListBox(core::rect<s32>(2, 22, 165, 88), fenster_renderer);
    gui_list_box->addItem(L"Diffuse");
    gui_list_box->addItem(L"Bump mapping");
    gui_list_box->addItem(L"Parallax mapping");
    gui_list_box->setSelected(1);

    // Rendererproblemtext
    problem_text = guie->addStaticText(
            L"Shader wird von Hardware nicht unterstützt.",
            core::rect<s32>(150 ,20 ,470 ,80));
    problem_text->setOverrideColor(video::SColor(100, 255, 255, 255));

    // Standardrenderer (parallax mapping wenn möglich)
    video::IMaterialRenderer* renderer = video_driver->getMaterialRenderer(video::EMT_PARALLAX_MAP_SOLID);
    if (renderer && renderer->getRenderCapability() == 0) gui_list_box->setSelected(2);
    return true;
}

void Systemview::reset_kamera() {
    camera = scenectrl->addCameraSceneNodeFPS(nullptr, 100.f, 0.5f); // Rotation, Translation
    camera->setPosition(core::vector3df(-200, 200, -200));
    camera->setFarValue(50000);
}

void Systemview::update() {
    // Kamerafokus
    if (kamerafokus >= 0) {
        try { // Planet fokussieren
            camera->setPosition(
                    node_planeten.at(kamerafokus - 1)->getPosition() + irr::core::vector3d(50.f, 0.f, 50.f)
            );
        } catch (std::exception& e) {
            kamerafokus = -1;
            reset_kamera();
        }
    }
}
