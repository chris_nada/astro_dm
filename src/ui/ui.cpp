#include "ui.h"

#include <iostream>
#include <dlib/algs.h>


irr::IrrlichtDevice*        UI::device;
irr::video::IVideoDriver*   UI::driver;
irr::gui::IGUIEnvironment*  UI::guie;
irr::gui::IGUISkin*         UI::skin;

irr::video::SColor UI::FARBE_HG = {0xFF, 0xA0, 0xA0, 0xA0};

bool UI::init() {
    std::cout << "UI::init()" << std::endl;

    device = irr::createDevice(
            irr::video::EDT_OPENGL,
            irr::core::dimension2d<irr::u32>(1280, 720),
            0,      // bits = 16
            false,  // fullscreen
            false,  // stencilbuffer
            true,   // vsync
            nullptr // Eventreceiver
    );

    if (!device) return false;
    driver = device->getVideoDriver();
    guie   = device->getGUIEnvironment();
    skin   = guie->getSkin();

    // Skin konfigurieren
    set_color(); // Transparenz 0x00 -> 0xFF

    // Fonts laden
    FONT_MONO_10  = UI::guie->getFont("data/font/10_mono.xml");
    FONT_MONO_12  = UI::guie->getFont("data/font/12_mono.xml");
    FONT_MONO_16  = UI::guie->getFont("data/font/16_mono.xml");
    FONT_SERIF_10 = UI::guie->getFont("data/font/10_serif.xml");
    FONT_SERIF_12 = UI::guie->getFont("data/font/12_serif.xml");
    FONT_SERIF_16 = UI::guie->getFont("data/font/16_serif.xml");
    if (FONT_SERIF_12) UI::skin->setFont(FONT_SERIF_12);

    // Fenstertitel
    std::wstring titel(L"Exominer [" + std::wstring(driver->getName()) + L"]");
    device->setWindowCaption(titel.c_str());
    device->setResizable(true);
    return true;
}

void UI::set_color() {
    for (irr::s32 i = 0; i < irr::gui::EGDC_COUNT ; ++i) {
        irr::video::SColor farbe = skin->getColor((irr::gui::EGUI_DEFAULT_COLOR)i);
        farbe.setAlpha(0xDD);
        farbe.setRed  (static_cast<irr::u32>(dlib::put_in_range(0x00, 0xFF, farbe.getRed()   + 0x20)));
        farbe.setGreen(static_cast<irr::u32>(dlib::put_in_range(0x00, 0xFF, farbe.getGreen() + 0x20)));
        farbe.setBlue (static_cast<irr::u32>(dlib::put_in_range(0x00, 0xFF, farbe.getBlue()  + 0x20)));
        skin->setColor((irr::gui::EGUI_DEFAULT_COLOR)i, farbe);
        skin->setColor(irr::gui::EGUI_DEFAULT_COLOR::EGDC_BUTTON_TEXT,     irr::video::SColor(0xFF, 0, 0, 0));
        skin->setColor(irr::gui::EGUI_DEFAULT_COLOR::EGDC_GRAY_TEXT,       irr::video::SColor(0xFF, 0, 0, 0));
        skin->setColor(irr::gui::EGUI_DEFAULT_COLOR::EGDC_HIGH_LIGHT_TEXT, irr::video::SColor(0xFF, 0, 0, 0));
    }
}

