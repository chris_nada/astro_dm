#pragma once

#if defined(_WIN32)
    #include <winsock2.h>
#endif
#include "../fenster.h"
#include "../../daten/host/host.h"
#include "../../daten/planet/planet.h"
#include "../3d/systemview.h"
#include <dlib/any/any_function.h>

/**
 * Hauptfenster der Anwendung. Zeigt die Tabs, die die Tabellen der Hosts und Planeten enthalten.
 * Aufgrund seiner Komplexität ist der Quelltext auf mehrere `cpp`-Dateien aufgeteilt.
 */
class Hauptfenster final : public Fenster {

public:

    /// Konstruktor, der auch Methoden zum Aufbauen aller Kindelemente des Fensters aufruft.
    Hauptfenster();

    /// Zeigt bis zum Beenden der Anwendung das Hauptfenster.
    void zeigen() override;

    /// Von Irrlicht vorgegebenes Interface zur Behandlung von Nutzerinteraktion.
    bool OnEvent(const irr::SEvent& event) override;

    /// Destuktor. Speichert aktuelle Konfiguration ab.
    virtual ~Hauptfenster();

protected:

    /// Führt der Reihe nach die Funktionen zum Aufbauen aller Kindelemente des Fensters aus.
    bool aufbauen() override;

private:

    /// Aktualisiert bzw. baut die Tabelle der Hosts frisch auf.
    void tabelle_host_erstellen();

    /// Aktualisiert bzw. baut die Tabelle der Planeten frisch auf.
    void tabelle_planet_erstellen();

    /// Aktualisiert bzw. baut das Tab mit den Optionen frisch auf. Sollte eigentlich nur 1x zu Beginn gebraucht werden.
    void tab_optionen_erstellen();

    /// *Versucht* zur Simulation zu springen des aktuell ausgewählten Host-Systems.
    void simulation();

    /// Führt einen Refresh der Daten in den Tabellen Host und Planet durch.
    void tabellen_reset();

    /// Bearbeitet Events vom Typ *Button*.
    bool event_button(const irr::SEvent& event);

    /// Bearbeitet Events vom Typ *Mouse*.
    bool event_mouse(const irr::SEvent& event);

    /// Bearbeitet Events vom Typ *Table*.
    bool event_table(const irr::SEvent& event);

    /// Bearbeitet Events vom Typ *ContextMenu*.
    bool event_kontextmenu(const irr::SEvent& event);

    /* Daten */

    /// In der Host-Tabelle darzustellende Hosts.
    std::vector<Host*> hosts;

    /// In der Planetentabelle darzustellende Planeten.
    std::vector<Planet*> planeten;

    /* UI Konfiguration */

    static constexpr const int padding = 8;
    int size_tab_x, size_tab_y;

    /* UI Elemente; hierarchisch eingerückt. */

    /// Pointer zum zuletzt erstellten Fenster.
    irr::gui::IGUIContextMenu*  menu;
    irr::gui::IGUITabControl*   tabctrl;
    irr::gui::IGUITab*              tab_host;
    irr::gui::IGUITable*                table_host;
    irr::gui::IGUITab*              tab_planet;
    irr::gui::IGUITable*                table_planet;
    irr::gui::IGUITab*              tab_optionen;
    irr::gui::IGUIEditBox*              filter_dist_input;
    irr::gui::IGUIEditBox*              suchen_input;
    irr::gui::IGUIComboBox*             combo_ki_kernel;
    irr::gui::IGUICheckBox*             chk_thumbs;
    irr::gui::IGUICheckBox*             chk_datentip;
    irr::gui::IGUICheckBox*             chk_phl_lesen;
    irr::gui::IGUIEditBox*              simbad_url1;
    irr::gui::IGUIEditBox*              simbad_url2;
    irr::gui::IGUIEditBox*              input_proxy;

    /// Aufzählung der IDs, die vom Kontextmenü zur Event-Zuordnung genutzt werden.
    enum KONTEXTMENU {
        PLANETEN_ZEIGEN = 0,
        SIMULATION_ZEIGEN,
        WEBSUCHE_SIMBAD,
        WEBSUCHE_ALADIN,
        DETAILS_HOST,
        DETAILS_PLANET
    };

    /// Aufzählung der IDs aller Knöpfe, die zur Event-Zuordnung genutzt werden.
    enum KNOPF {
        EXPORT_HOSTS = 0,
        FILTER,
        SUCHE,
        SUCHE_RESET,
        KI_HOST,
        KI_HOST_RESET,
        KI_PLANET,
        SAVE_NETZWERK_CONF,
        SYNC_CACHE,
        UPDATE_HOSTS,
        UPDATE_PLANETEN,
        PHL_DOWNLOAD,
        PHL_EINLESEN,
        CACHE_HOSTS
    };

    void event_key(const SEvent& event);
};