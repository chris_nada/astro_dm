#include "hauptfenster.h"

bool Hauptfenster::OnEvent(const irr::SEvent& event) {
    if (event.EventType == irr::EET_GUI_EVENT) { // TODO resize

        // Kontextmenü
        if (event.GUIEvent.EventType == irr::gui::EGET_MENU_ITEM_SELECTED) {
            (void) event_kontextmenu(event);
        }

        // Buttons
        else if (event.GUIEvent.EventType == irr::gui::EGET_BUTTON_CLICKED) {
            (void) event_button(event);
        }

        // Tabellenevents
        else if (event.GUIEvent.EventType == irr::gui::EGET_TABLE_HEADER_CHANGED) {
            (void) event_table(event);
        }
    }

    // Maus-Events
    else if (event.EventType == irr::EET_MOUSE_INPUT_EVENT) {
        (void) event_mouse(event);
    }

    // Keyboard-Events
    else if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
        (void) event_key(event);
    }
    return false;
}
