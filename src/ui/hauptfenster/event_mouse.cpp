#include "hauptfenster.h"
#include "../../daten/io/net.h"
#include "../../daten/io/simbad.h"

#include <dlib/unicode/unicode.h>

bool Hauptfenster::event_mouse(const irr::SEvent& event) {

    // Rechtsklick
    if (event.MouseInput.Event == irr::EMIE_RMOUSE_LEFT_UP) {

        // Kontextmenü Host
        if (tabctrl->getAbsolutePosition().isPointInside({event.MouseInput.X, event.MouseInput.Y})) {
            if (tabctrl->getActiveTab() == tab_host->getNumber() && table_host->getSelected() >= 0) {
                Host* selected_host = (Host*) table_host->getCellData(table_host->getSelected(), 0);
                if (!selected_host) return false;

                // Host* legitim -> Kontextmenü erzeugen
                menu = UI::guie->addContextMenu(irr::core::rect<irr::s32>(0, 0, 0x40, 0x40));
                menu->addItem(L"Alternativnamen", -1, true, true);
                for (const std::string& name : selected_host->get_namen()) {
                    menu->getSubMenu(0)->addItem(dlib::convert_mbstring_to_wstring(name).c_str());
                }
                menu->addItem(L"Websuche", -1, true, true);
                menu->getSubMenu(1)->addItem(L"SIMBAD", WEBSUCHE_SIMBAD); // TODO Position weiter rechts
                menu->getSubMenu(1)->addItem(L"ALADIN", WEBSUCHE_ALADIN);
                menu->addItem(L"Details", DETAILS_HOST);

                if (selected_host->is_darstellbar()) menu->addItem(L"Simulation", SIMULATION_ZEIGEN);

                // Thumb erstellen
                if (!selected_host->get_simbad_oid().empty() && chk_thumbs->isChecked()) {
                    //menu->addSeparator();

                    auto image = selected_host->get_image();

                    if (image) {
                        auto thumb = UI::guie->addImage(image, {0, (irr::s32) menu->getItemCount() * 20});
                        menu->addChild(thumb);
                        menu->setMinSize({static_cast<u32>(thumb->getAbsolutePosition().getWidth()),
                                          static_cast<u32>(menu->getAbsolutePosition().getHeight() +
                                                           thumb->getAbsolutePosition().getHeight())
                        });
                    }
                }
                menu->setRelativePosition({event.MouseInput.X, event.MouseInput.Y - 0x20});
                menu->setCloseHandling(irr::gui::ECMC_REMOVE);
                return true;
            }

            // Kontextmenü Planet
            else if (tabctrl->getActiveTab() == tab_planet->getNumber() && table_planet->getSelected() >= 0) {
                Planet* selected_planet = (Planet*) table_planet->getCellData(table_planet->getSelected(), 0);
                if (!selected_planet) return false;

                // Planet* legitim -> Kontextmenü erzeugen
                menu = UI::guie->addContextMenu(irr::core::rect<irr::s32>(0, 0, 0x40, 0x40));
                menu->addItem(L"Details", DETAILS_PLANET);

                // Einstellungen
                menu->setRelativePosition({event.MouseInput.X, event.MouseInput.Y - 0x20});
                menu->setCloseHandling(irr::gui::ECMC_REMOVE);
                return true;
            }
        }
    }
    return true;
}
