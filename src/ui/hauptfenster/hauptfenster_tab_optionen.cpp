#include "hauptfenster.h"
#include "../../daten/ki/host_ki.h"
#include "../../daten/io/config.h"
#include "../../daten/io/simbad.h"
#include "../../daten/io/net.h"

void Hauptfenster::tab_optionen_erstellen() {
    std::cout << "Hauptfenster.tab_optionen_erstellen()" << std::endl;

    // Liefert die nächste Position für einen Button.
    auto get_pos = [top_left = tab_optionen->getAbsolutePosition().UpperLeftCorner](int x, int y) {
        return irr::core::rect<s32> {
                top_left.X     + x*260, top_left.Y-30 + y*30,
                top_left.X+240 + x*260, top_left.Y-6  + y*30
        };
    };

    // Position des UI-Elements im 'Raster'
    int x = 0, y = 0;

    // KI
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::KI_HOST,
                        L"Regression - Hostparameter",
                        L"Wendet einen Regressionsalgorithmus auf Hostparameter an.");
    combo_ki_kernel = UI::guie->addComboBox(get_pos(x+1, y-1), tab_optionen, 555);
    combo_ki_kernel->addItem(L"Histogrammintersektion", Host_KI::KERNEL::HISTOGRAMMINTERSEKTION);
    combo_ki_kernel->addItem(L"Linear",                 Host_KI::KERNEL::LINEAR);
    combo_ki_kernel->addItem(L"Polynomial",             Host_KI::KERNEL::POLYNOMIAL);
    combo_ki_kernel->addItem(L"Radialbasis",            Host_KI::KERNEL::RADIALBASIS);
    combo_ki_kernel->addItem(L"Sigmoid",                Host_KI::KERNEL::SIGMOID);

    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::KI_PLANET,
                        L"Regression - Planetparameter",
                        L"Wendet einen Regressionsalgorithmus auf Planetparameter an.");
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::KI_HOST_RESET,
                        L"Reset - Hostparameter",
                        L"Setzt von der KI gesetzte Werte der Hosts zurück.");

    // Filter - Distanz
    y++;
    UI::guie->addStaticText(
            L"Distanz [ly] Max", get_pos(x, y++),false, false, tab_optionen);
    filter_dist_input = UI::guie->addEditBox(
            L"100", get_pos(x, y++), true, tab_optionen);
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::FILTER,
                        L"Filter anwenden", L"Wendet eingestellte Filter an.");

    // Suche
    irr::core::rect<s32> pos(UI::driver->getScreenSize().Width - 248, 8, UI::driver->getScreenSize().Width - 8, 32);
    UI::guie->addButton(pos, nullptr, KNOPF::SUCHE, L"Suchen", L"Durchsucht Tabelle nach gegebenem String.");
    suchen_input = UI::guie->addEditBox(L"", pos.operator-=({248, 0}), true);
    irr::core::rect<s32> pos2(pos.UpperLeftCorner.X - 40, pos.UpperLeftCorner.Y,
                              pos.UpperLeftCorner.X - 8, pos.LowerRightCorner.Y);
    UI::guie->addButton(pos2, nullptr, KNOPF::SUCHE_RESET, L"X", L"Suche zurücksetzen.");


    // Sync update
    x = 2; y = 0;
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::SYNC_CACHE,
                        L"Daten lesen",
                        L"Lädt lokal vorhandene Host- und Planetdaten.\nEvtl. nicht vorhandene Hosts werden heruntergeladen.\nSTRG + L");
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::UPDATE_PLANETEN,
                        L"Update Planeten",
                        L"Führt ein vollständiges Update der Planetendaten durch.\nNeu referenzierte Hosts werden ebenfalls heruntergeladen.");
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::UPDATE_HOSTS,
                        L"Update Hosts",
                        L"Führt ein vollständiges Update aller Hostdaten durch.\nDieser Prozess kann sehr lange dauern.");

    // PHL Daten
    y++;
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::PHL_DOWNLOAD,
                        L"Download PHL-Datenbank",
                        L"Lädt die aktuellste Datenbank des Arecibo\nPlanetary Habitability Laboratorys herunter.");
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::PHL_EINLESEN,
                        L"PHL-Daten lesen",
                        L"Lädt die PHL-Datenbank in den Speicher."
                        L"\nBereits eingelesene Daten aus möglicherweise\nanderen Quellen werden dabei überschrieben.");
    chk_phl_lesen = UI::guie->addCheckBox(Config::get_bool("PHL_LESEN"), get_pos(x, y++), tab_optionen, -1,
                        L"PHL Immer lesen");


    // Export
    y++;
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::EXPORT_HOSTS,
                        L"Export Hosts", L"Exportiert den aktuellen Stand der Hostdaten in eine CSV-Datei.");
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::CACHE_HOSTS,
                        L"Hosts Cachen", L"Alle importierten Host-Daten werden gecacht,\n"
                                          "was das Einlesen enorm beschleunigt.");

    // Simbad Host + Mirror
    y++;
    UI::guie->addStaticText(L"Proxy", get_pos(x, y++), false, false, tab_optionen);
    input_proxy = UI::guie->addEditBox(dlib::convert_mbstring_to_wstring(Net::PROXY).c_str(), get_pos(x, y++), true, tab_optionen);
    UI::guie->addStaticText(L"SIMBAD Host", get_pos(x, y++), false, false, tab_optionen);
    simbad_url1 = UI::guie->addEditBox(dlib::convert_mbstring_to_wstring(Simbad::URL1).c_str(), get_pos(x, y++), true, tab_optionen);
    UI::guie->addStaticText(L"SIMBAD Mirror", get_pos(x, y++), false, false, tab_optionen);
    simbad_url2 = UI::guie->addEditBox(dlib::convert_mbstring_to_wstring(Simbad::URL2).c_str(), get_pos(x, y++), true, tab_optionen);
    UI::guie->addButton(get_pos(x, y++), tab_optionen, KNOPF::SAVE_NETZWERK_CONF,
                        L"Übernehmen", L"Speichert gebene Netzwerkkonfiguration.");

    // Weitere Optionen
    x = 4; y = 0;
    chk_datentip = UI::guie->addCheckBox(Config::get_bool("TIP_DATEN"),  get_pos(x, y++), tab_optionen, -1, L"Hilfe zeigen");
    chk_thumbs   = UI::guie->addCheckBox(Config::get_bool("THUMBNAILS"), get_pos(x, y++), tab_optionen, -1, L"Thumbnails");
}
