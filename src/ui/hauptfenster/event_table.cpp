#include "hauptfenster.h"

bool Hauptfenster::event_table(const irr::SEvent& event) {
    irr::gui::IGUITable* table = (irr::gui::IGUITable*) event.GUIEvent.Caller;

    // Hosttabelle
    if (table == table_host) {

        // Sortierfunktionen
        auto sort_name = [&](Host* a, Host* b) { return a->get_name()            < b->get_name(); };
        auto sort_plan = [&](Host* a, Host* b) { return a->get_planeten().size() < b->get_planeten().size(); };
        auto sort_dist = [&](Host* a, Host* b) { return a->data().dist()         < b->data().dist(); };
        auto sort_r    = [&](Host* a, Host* b) { return a->data().r()            < b->data().r(); };
        auto sort_m    = [&](Host* a, Host* b) { return a->data().m()            < b->data().m(); };
        auto sort_T    = [&](Host* a, Host* b) { return a->data().temp()         < b->data().temp(); };
        auto sort_spec = [&](Host* a, Host* b) { return a->data().spektraltyp.get_beschreibung() <
                                                        b->data().spektraltyp.get_beschreibung(); };
        // Ausgewählte Spalte
        switch (table->getActiveColumn()) {
            case 0: std::sort(hosts.begin(), hosts.end(), sort_name); break;
            case 1: std::sort(hosts.begin(), hosts.end(), sort_plan); break;
            case 4: std::sort(hosts.begin(), hosts.end(), sort_dist); break;
            case 5: std::sort(hosts.begin(), hosts.end(), sort_spec); break;
            case 6: std::sort(hosts.begin(), hosts.end(), sort_r   ); break;
            case 7: std::sort(hosts.begin(), hosts.end(), sort_m   ); break;
            case 8: std::sort(hosts.begin(), hosts.end(), sort_T   ); break;
            default:break;
        }
        tabelle_host_erstellen();
        return true;
    }
    return false;
}
