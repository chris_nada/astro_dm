#include <dlib/misc_api.h>
#include <dlib/string.h>

#include "hauptfenster.h"
#include "../../daten/io/config.h"
#include "../../daten/io/simbad.h"
#include "../../daten/io/net.h"

Hauptfenster::Hauptfenster() : hosts(Host::alle_unique()), planeten(Planet::alle()) {
    UI::device->setEventReceiver(this);
    (void) aufbauen();

    // Nachricht über keine Daten
    if (Config::get_bool("TIP_DATEN") && planeten.empty() && hosts.empty()) {
        UI::guie->addMessageBox(L"Leere Datenbasis",
           L"Bei Programmstart werden Daten nicht (mehr) automatisch geladen.\n\n"
            "Über 'Optionen' -> 'Daten laden' werden lokal bereits gespeicherte Daten eingelesen, während\n"
            "über 'Optionen' -> 'Daten Update' die aktuellste Datenbasis online eingeholt wird.\n"
            "Letzterer Prozess kann je nach Internetverbindung sehr zeitintensiv sein.");
        Config::speichern("TIP_DATEN", "0");
    }
    zeigen();
}

Hauptfenster::~Hauptfenster() {
    std::cout << "~Hauptfenster()" << std::endl;
    std::map<std::string, std::string> konfiguration {
        {"THUMBNAILS",  std::to_string(chk_thumbs->isChecked())},
        {"TIP_DATEN",   std::to_string(chk_datentip->isChecked())},
        {"PHL_LESEN",   std::to_string(chk_phl_lesen->isChecked())},
        {"SIMBAD_URL1", Simbad::URL1},
        {"SIMBAD_URL2", Simbad::URL2},
        {"PROXY",       Net::PROXY}
    };
    Config::speichern(konfiguration);
}

bool Hauptfenster::aufbauen() {
    std::cout << "Hauptfenster.aufbauen()" << std::endl;
    // Tabs Setup
    size_tab_x = UI::driver->getScreenSize().Width  - padding;
    size_tab_y = UI::driver->getScreenSize().Height - padding;
    tabctrl    = UI::guie->addTabControl(irr::core::rect<irr::s32>(padding, padding, size_tab_x, size_tab_y));
    tab_host     = tabctrl->addTab(L"Hosts");
    tab_planet   = tabctrl->addTab(L"Planeten");
    tab_optionen = tabctrl->addTab(L"Optionen");
    table_host   = UI::guie->addTable({padding, padding, size_tab_x - padding, size_tab_y - 44}, tab_host);
    table_planet = UI::guie->addTable({padding, padding, size_tab_x - padding, size_tab_y - 44}, tab_planet);

    // Tabellen
    tabelle_host_erstellen();
    tabelle_planet_erstellen();

    // Optionstab
    tab_optionen_erstellen();
    return true;
}

void Hauptfenster::zeigen() {
    while(UI::device->run()) {
        UI::driver->beginScene(true, true, UI::FARBE_HG);
        UI::guie->drawAll();
        UI::driver->endScene();
        dlib::sleep(1);
    }
}

void Hauptfenster::tabelle_host_erstellen() {
    std::cout << "Hauptfenster.tabelle_host_erstellen(" << hosts.size() << ")" << std::endl;
    table_host->clear();

    // Überschriften
    unsigned int col = 0;
    table_host->addColumn(L"Host",        col++);
    table_host->addColumn(L"Planeten",    col++);
    table_host->addColumn(L"α [°]",       col++);
    table_host->addColumn(L"δ [°]",       col++);
    table_host->addColumn(L"d [ly]",      col++);
    table_host->addColumn(L"Spektraltyp", col++);
    table_host->addColumn(L"r [r⊙]",      col++);
    table_host->addColumn(L"m [m⊙]",      col++);
    table_host->addColumn(L"T [K]",       col++);
    table_host->addColumn(L"Alter [Gyr]", col++);

    // Daten füllen
    Ladefenster ladefenster(L"Tabelle <Host> wird erstellt...", hosts.size() - 1);
    for (unsigned int i = 0; i < hosts.size(); ++i) {
        col = 0;
        const Hostwerte& data = hosts[i]->data();
        table_host->addRow(i);
        table_host->setCellData(i, col,   (void*) hosts[i]);
        table_host->setCellText(i, col++, hosts[i]->get_name().c_str());
        table_host->setCellColor(i, 0, {0xFF, static_cast<u32>(0xFF - 0xFF * hosts[i]->is_darstellbar()), 0x00, 0x00});
        table_host->setCellText(i, col++, std::to_string(hosts[i]->get_planeten().size()).c_str());
        table_host->setCellText(i, col++, data.ra.str().c_str());
        table_host->setCellText(i, col++, data.dek.str().c_str());
        table_host->setCellText(i, col++, data.dist.str().c_str());
        if (data.dist.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        table_host->setCellText(i, col++, data.spektraltyp.get_beschreibung().c_str());
        if (data.spektraltyp.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        table_host->setCellText(i, col++, data.r.str().c_str());
        if (data.r.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        table_host->setCellText(i, col++, data.m.str().c_str());
        if (data.m.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        table_host->setCellText(i, col++, data.temp.str().c_str());
        if (data.temp.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        table_host->setCellText(i, col++, data.alter.str().c_str());
        if (data.alter.is_vorhersage()) table_host->setCellColor(i, col-1, {0xFF, 0x00, 0x00, 0xFF});
        if (i % 0x10 == 0) ladefenster.update(i);
    }
    for (int i = 0; i < table_host->getColumnCount(); ++i) table_host->setColumnWidth(i, 120);
}

void Hauptfenster::tabelle_planet_erstellen() {
    std::cout << "Hauptfenster.tabelle_planet_erstellen(" << planeten.size() << ")" << std::endl;
    table_planet->clear();

    // Überschriften
    unsigned int col = 0;
    table_planet->addColumn(L"Host",           col++);
    table_planet->addColumn(L"Letter",         col++);
    table_planet->addColumn(L"Masse [m⊕]",     col++);
    table_planet->addColumn(L"Radius [r⊕]",    col++);
    table_planet->addColumn(L"Dichte [g/cm³]", col++);
    table_planet->addColumn(L"T [K]",          col++);
    //table_planet->addColumn(L"Monde",          col++);
    table_planet->addColumn(L"Orbit p [d]",    col++);
    table_planet->addColumn(L"Orbit e",        col++);
    table_planet->addColumn(L"Orbit α [AU]",   col++);

    // Daten füllen
    Ladefenster ladefenster(L"Tabelle <Planeten> wird erstellt...", planeten.size() - 1);
    for (unsigned int i = 0; i < planeten.size(); ++i) {
        col = 0;
        table_planet->addRow(i);
        table_planet->setCellData(i, col,   (void*) planeten[i]);

        // Existiert Host? Nein -> rot
        table_planet->setCellText(i, col++, planeten[i]->get_host_name().c_str());
        if (!Host::get(planeten[i]->get_host_name())) table_planet->setCellColor(i, 0, {0xFF, 0xFF, 0x00, 0x00});

        // Daten auslesen
        const Planetwerte& data = planeten[i]->data();
        std::string letter({data.letter(), '\0'});
        table_planet->setCellText(i, col++, letter.c_str());
        if (planeten[i]->get_quelle() != Planet::Quelle::NASA) table_planet->setCellColor(i, 1, {0xFF, 0x00, 0x00, 0xFF});
        table_planet->setCellText(i, col++, data.m.c_str());
        table_planet->setCellText(i, col++, data.r.c_str());
        table_planet->setCellText(i, col++, data.dichte.c_str());
        table_planet->setCellText(i, col++, data.temp.c_str());
        //table_planet->setCellText(i, col++, data.n_monde.c_str());
        table_planet->setCellText(i, col++, data.orbit_p.c_str());
        table_planet->setCellText(i, col++, data.orbit_e.c_str());
        table_planet->setCellText(i, col++, data.orbit_a.c_str());
        if (i % 0x10 == 0) ladefenster.update(i);
    }
    for (int i = 0; i < table_planet->getColumnCount(); ++i) table_planet->setColumnWidth(i, 120);
}

void Hauptfenster::simulation() {
    std::cout << "Hauptfenster::simulation()" << std::endl;
    std::vector<Host*> hosts;
    std::vector<Planet*> planeten;

    // Host sammeln
    { Host* host = (Host*) table_host->getCellData(table_host->getSelected(), 0);
    if (host) hosts.push_back(host); }

    // Planeten sammeln
    for (Host* host : hosts) for (Planet* planet : host->get_planeten()) planeten.push_back(planet);

    // Simulation los
    if (!hosts.empty() && !planeten.empty()) {
        UI::guie->clear();
        { Systemview sim(hosts, planeten); }
        UI::device->setEventReceiver(this);
        UI::device->getCursorControl()->setVisible(true);
        aufbauen();
    }
    else std::cerr << "\tSimulation nicht moeglich, Host oder Planeten nicht gefunden." << std::endl;
}

void Hauptfenster::tabellen_reset() {
    hosts = Host::alle_unique();
    planeten = Planet::alle();
    tabelle_host_erstellen();
    tabelle_planet_erstellen();
}

