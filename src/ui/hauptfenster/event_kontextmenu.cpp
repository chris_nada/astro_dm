#include "hauptfenster.h"
#include "../../daten/io/etc.h"
#include "../detailfenster/hostfenster.h"
#include "../detailfenster/planetfenster.h"

bool Hauptfenster::event_kontextmenu(const irr::SEvent& event) {
    if (auto temp_menu = (irr::gui::IGUIContextMenu*) event.GUIEvent.Caller) {

        // Host Kontextmenü
        if (table_host->getSelected() >= 0 && tabctrl->getTab(tabctrl->getActiveTab()) == tab_host) {
            Host* selected_host = (Host*) table_host->getCellData(table_host->getSelected(), 0);
            if (!selected_host) return false;
            switch (temp_menu->getItemCommandId(temp_menu->getSelectedItem())) {
                case PLANETEN_ZEIGEN: /* TODO */ return true;
                case SIMULATION_ZEIGEN: simulation(); return true;
                case WEBSUCHE_SIMBAD: {
                    Etc::browser("http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=" + selected_host->get_name());
                    return true;
                }
                case WEBSUCHE_ALADIN: {
                    Etc::browser("http://aladin.u-strasbg.fr/AladinLite/?target=" + selected_host->get_name());
                    return true;
                }
                case DETAILS_HOST: {
                    Hostfenster hostfenster(this, selected_host);
                    return true;
                }
                default: break;
            }
        }
        // Planet Kontextmenü
        else if (table_planet->getSelected() >= 0 && tabctrl->getTab(tabctrl->getActiveTab()) == tab_planet) {
            Planet* selected_planet = (Planet*) table_planet->getCellData(table_planet->getSelected(), 0);
            if (!selected_planet) return false;
            switch (temp_menu->getItemCommandId(temp_menu->getSelectedItem())) {
                case DETAILS_PLANET: {
                    Planetfenster planetfenster(this, selected_planet);
                    return true;
                }
                default: break;
            }
        }
    }
    return false;
}
