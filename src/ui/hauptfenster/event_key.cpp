#include "hauptfenster.h"
#include "../../daten/io/sync.h"

void Hauptfenster::event_key(const SEvent& event) {

    // Strg + L
    if (event.KeyInput.Control && event.KeyInput.Key == EKEY_CODE::KEY_KEY_L) {
        static size_t zaehler = 0;
        ++zaehler;
        if  (zaehler % 2 != 0) return; // Event wird 2x gefeuert => nur 1x abfangen
        Sync::sync();
        tabellen_reset();
    }
}
