#include "hauptfenster.h"
#include "../../daten/ki/host_ki.h"
#include "../../daten/io/export.h"
#include "../../daten/io/net.h"
#include "../../daten/io/sync.h"
#include "../../daten/io/simbad.h"
#include "../../daten/io/phl.h"

#include <dlib/gui_widgets.h>

bool Hauptfenster::event_button(const irr::SEvent& event) {
    auto button = (irr::gui::IGUIButton*) event.GUIEvent.Caller;

    switch (button->getID()) {

        // Regression Host
        case KNOPF::KI_HOST: {
            irr::core::stringw text = Host_KI::los(
                    static_cast<Host_KI::KERNEL>(combo_ki_kernel->getItemData(combo_ki_kernel->getSelected())
                    ));
            UI::guie->addMessageBox(L"Statusbericht", text.c_str());
            tabelle_host_erstellen(); // Refresh
        } break;

            // Regression Planet
        case KNOPF::KI_PLANET:
            // TODO
            std::cout << "btn_planet_ki geklickt" << std::endl;
            break;

            // Filter
        case KNOPF::FILTER: {
            // Distanz
            double max = wcstod(filter_dist_input->getText(), nullptr);
            std::cout << max << std::endl;
            for (unsigned int i = 0; i < hosts.size(); ++i) {
                Host* host = hosts[i];
                if (host->data().dist() > max) {
                    hosts.erase(hosts.begin() + i);
                    --i;
                }
            }
            for (unsigned int i = 0; i < hosts.size(); ++i) {
                Planet* planet = planeten[i];
                if (Host::get(planet->get_host_name()) &&
                    Host::get(planet->get_host_name())->data().dist() > max) {
                    planeten.erase(planeten.begin() + i);
                    --i;
                }
            }
            tabelle_host_erstellen();
            tabelle_planet_erstellen();
        } break;

            // Suchen
        case KNOPF::SUCHE: {
            std::string suchterm(dlib::tolower(dlib::convert_wstring_to_mbstring(suchen_input->getText())));
            if (suchterm.empty()) break; // Leer - abbruch
            std::cout << "SUCHE: " << suchterm << std::endl;
            if (tabctrl->getTab(tabctrl->getActiveTab()) == tab_host) { // Hostsuche
                for (unsigned int i = 0; i < hosts.size(); ++i) {
                    Host* host = hosts[i];
                    std::string hostname; // alle Hostnamen berücksichtigen
                    for (const std::string& name : host->get_namen()) hostname += dlib::tolower(name);
                    if (hostname.find(suchterm) != std::string::npos) continue;
                    else { hosts.erase(hosts.begin() + i); --i; }
                }
                tabelle_host_erstellen();
            }
            else if (tabctrl->getTab(tabctrl->getActiveTab()) == tab_planet) { // Planetsuche
                for (unsigned int i = 0; i < planeten.size(); ++i) {
                    if (dlib::tolower(planeten[i]->get_name()).find(suchterm) != std::string::npos) continue;
                    else { planeten.erase(planeten.begin() + i); --i; }
                }
                tabelle_planet_erstellen();
            }
        } break;

            // Suche - Reset
        case KNOPF::SUCHE_RESET:
            tabellen_reset();
            break;

            // SIMBAD URLs speichern
        case KNOPF::SAVE_NETZWERK_CONF:
            Simbad::URL1 = dlib::convert_wstring_to_mbstring(simbad_url1->getText());
            Simbad::URL2 = dlib::convert_wstring_to_mbstring(simbad_url2->getText());
            Net::PROXY   = dlib::convert_wstring_to_mbstring(input_proxy->getText());
            break;

            // Sync & Update
        case KNOPF::SYNC_CACHE:
            Sync::sync();
            tabellen_reset();
            break;
        case KNOPF::UPDATE_HOSTS:
            Host::einlesen(true);
            Sync::sync();
            tabellen_reset();
            break;
        case KNOPF::UPDATE_PLANETEN:
            Net::download(Sync::URL_PLANETEN, Sync::DATEI_PLANETEN);
            Sync::sync();
            tabellen_reset();
            break;
        case KNOPF::PHL_DOWNLOAD:
            if (PHL::download()) UI::guie->addMessageBox(L"Status", L"Download erfolgreich abgeschlossen.");
            else UI::guie->addMessageBox(L"Fehler", L"Datei konnte nicht heruntergeladen werden.");
            break;
        case KNOPF::PHL_EINLESEN:
            if (PHL::einlesen()) tabellen_reset();
            else UI::guie->addMessageBox(L"Fehler", L"PHL Datenbank nicht vorhanden.");
            break;

            // Export Hostwerte CSV
        case KNOPF::EXPORT_HOSTS: {
            auto f_lambda = [&](const std::string& pfad) {
                if (!Export::csv(hosts, pfad)) {
                    UI::guie->addMessageBox(L"Fehler",
                                            L"Datei konnte nicht geschrieben werden: Kein Zugriff auf Pfad.");
                }
            };
            dlib::save_file_box(f_lambda); // stattdessen möglich: UI::guie->addFileOpenDialog(L"...");
        } break;

        case KNOPF::CACHE_HOSTS: {
            (void) Host::cache_schreiben(); // TODO Rückgabe ignoriert
        } break;

            // Reset Hostwerte
        case KNOPF::KI_HOST_RESET:
            for (const auto& host : hosts) host->data().reset();
            tabelle_host_erstellen();
            break;

        default: break;
    }
    return true;
}
