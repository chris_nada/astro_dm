#pragma once

#include <IGUIWindow.h>
#include <IGUIStaticText.h>

/**
 * Ein einfaches Ladefenster für die Nutzeroberfläche, was nur eine Textanzeige mit dem Fortschritt in
 * Prozent beinhaltet.
 * Das Fenster erzeugt sich automatisch und verschwindet automatisch.
 * @warning Das Fenster verschwindet nur automatisch, wenn ein Update mit dem Endwert durchgeführt wird.
 */
class Ladefenster final {

public:

    /// Konstruktor, der das Fenster anzeigt und
    explicit Ladefenster(const irr::core::stringw& titel, unsigned int total);

    /// Destruktor der sich um das entfernen der UI-Elemente kümmert.
    virtual ~Ladefenster();

    /// Führt ein Update durch oder erzeugt das Ladefenster, falls es noch nicht angezeigt wird.
    void update(unsigned int fortschritt);

    /// Führt ein Update durch oder erzeugt das Ladefenster, falls es noch nicht angezeigt wird.
    void update();

    /// Zum nachträglichen Verändern des Zielfortschritts.
    inline void set_total(unsigned int total) { Ladefenster::total = total; }

private:

    /// Wieviele Ladefenster gleichzeitig offen sind.
    static inline int anzahl = 0;

    /// Gibt an, wie weit der Fortschritt momentan ist.
    unsigned int jetzt;

    /// Gibt an, welcher Wert 100% repräsentiert.
    unsigned int total;

    /// Interner Zeiger zu dem GUI-Element `Fenster`.
    irr::gui::IGUIWindow* fenster;

    /// Interner Zeiger zu dem angezeigten Text.
    irr::gui::IGUIStaticText* text;

};
