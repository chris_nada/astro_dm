#include "hostfenster.h"

#include <dlib/unicode.h>

Hostfenster::Hostfenster(IEventReceiver* parent, Host* host) : Fenster(parent), host(host) {
    std::cout << "Hostfenster(" << host->get_name() << ')' << std::endl;
    aufbauen();
}

bool Hostfenster::aufbauen() {

    // Dimensionen
    const auto mx = UI::driver->getScreenSize().Width/2;
    const auto my = UI::driver->getScreenSize().Height/2;
    const unsigned int breite   = mx;
    const unsigned int hoehe    = static_cast<unsigned int>(my * 1.5);
    static constexpr int margin = 8; // Layoutmargins

    // Fenster erzeugen
    fenster = UI::guie->addWindow({
        static_cast<int>(mx  - breite / 2),
        static_cast<int>(my -  hoehe  / 2),
        static_cast<int>(mx  + breite / 2),
        static_cast<int>(my +  hoehe  / 2)
        }, false, dlib::convert_mbstring_to_wstring("Details: " + host->get_name()).c_str()
    );

    // Liefert das nächste Rechteck zum Platzieren von Inhalten
    int zaehler = 0;
    auto next_rect = [=, &zaehler] (const int x = 0) {
        static constexpr int padding = 20;
        ++zaehler;
        irr::core::rect<irr::s32> rect(x + margin, margin + padding * zaehler,
                                       breite - margin, margin + padding * zaehler + padding
        );
        return rect;
    };

    // Fügt dem Fenster gegebenen String als Text hinzu
    auto text = [&] (const std::string& mb_string, const int x = 0) {
        auto text_ptr = UI::guie->addStaticText(
                dlib::convert_mbstring_to_wstring(mb_string).c_str(), next_rect(x), false, true, fenster
        );
        text_ptr->setOverrideFont(UI::FONT_MONO_16);
        text_ptr->setOverrideColor({0xFF, 0x0, 0x0, 0x0});
        return text_ptr;
    };

    // Daten anzeigen
    const Hostwerte& data = host->data();
    text("Namen");
    for (const std::string& name : host->get_namen()) text(name, 24);
    text("Position ra/dek = " + data.ra.str() + " / " + data.dek.str());
    text("Spektraltyp = " + data.spektraltyp.get_beschreibung());
    text("Eigenbewegung (mas/y) = " + std::to_string(data.eigenbewegung_abs()));
    text("Distanz (ly) = " + data.dist.str());
    text("Temperatur (K) = " + data.temp.str());
    text("Radius (r(s)) = " + data.r.str());
    text("Masse (m(s)) = " + data.m.str());

    // Bild anzeigen
    auto image = host->get_image();
    if (image) {
        auto padding_text = text("", margin);
        auto thumb = UI::guie->addImage(image, {0, padding_text->getRelativePosition().LowerRightCorner.Y});
        fenster->addChild(thumb);
    }

    return true;
}

void Hostfenster::zeigen() {
    // nicht genutzt
}

bool Hostfenster::OnEvent(const irr::SEvent& event) {
    (void) event;
    return false;
}
