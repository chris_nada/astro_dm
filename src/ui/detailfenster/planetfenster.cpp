#include "planetfenster.h"

Planetfenster::Planetfenster(irr::IEventReceiver* parent, Planet* planet) : Fenster(parent), planet(planet) {
    std::cout << "Planetfenster(" << planet->get_name() << ')' << std::endl;
    aufbauen();
}

bool Planetfenster::aufbauen() {
    std::cout << "Planetfenster::" << __func__ << '(' << planet->get_name() << ')' << std::endl;

    // Dimensionen
    const auto mx = UI::driver->getScreenSize().Width/2;
    const auto my = UI::driver->getScreenSize().Height/2;
    const unsigned int   breite = mx;
    const unsigned int   hoehe  = static_cast<unsigned int>(my * 1.5);
    static constexpr int margin = 8; // Layoutmargins

    // Fenster erzeugen
    fenster = UI::guie->addWindow({
        static_cast<int>(mx - breite / 2),
        static_cast<int>(my - hoehe  / 2),
        static_cast<int>(mx + breite / 2),
        static_cast<int>(my + hoehe  / 2)
        }, false, dlib::convert_mbstring_to_wstring("Details " + planet->get_host_name()).c_str()
    );

    // Alle Planeten des Hosts sammeln
    std::vector<Planet*> planeten = { planet };
    Host* host = Host::get(planet->get_host_name());
    if (host) for (const auto& p : host->get_planeten()) {
        if (std::find(planeten.begin(), planeten.end(), p) == planeten.end()) {
            planeten.push_back(p);
        }
    }

    // Planeten nach Letter sortieren
    std::sort(planeten.begin(), planeten.end(),
            [] (const auto& ptr1, const auto& ptr2) -> bool {
        return ptr1->data().letter() < ptr2->data().letter();
    });

    // Tab für jeden Planet
    auto tabs = UI::guie->addTabControl({margin, 30, (int) breite - 2 * margin, (int) hoehe - 2 * margin}, fenster);
    for (Planet* p : planeten) {
        char letter = p->data().letter();
        std::string letter_str(&letter, 1);
        auto tab = tabs->addTab(dlib::convert_mbstring_to_wstring(letter_str).c_str());

        // Datentabelle erstellen
        auto table = UI::guie->addTable({margin, margin, (int) breite - 2 * margin, (int) hoehe - 2 * margin}, tab);
        table->addColumn(L"Bezeichnung");
        table->addColumn(L"Wert");
        table->setColumnWidth(0, static_cast<irr::u32>(breite / 2.25));
        table->setColumnWidth(1, static_cast<irr::u32>(breite / 2.25));

        // Daten eintragen
        unsigned int i = 0;
        for (const auto& wertepaar : p->data().als_map()) {
            table->addRow(i);
            table->setCellText(i, 0, dlib::convert_mbstring_to_wstring(wertepaar.first).c_str());
            table->setCellText(i, 1, dlib::convert_mbstring_to_wstring(wertepaar.second).c_str());
            ++i;
        }
    }
    return true;
}

void Planetfenster::zeigen() {
    // nicht genutzt
}

bool Planetfenster::OnEvent(const irr::SEvent& event) {
    (void) event;
    return false;
}
