#pragma once

#include "../../daten/host/host.h"
#include "../fenster.h"

/**
 * Bei dieser Klasse handelt es sich um das Fenster, welches eine detaillierte
 * Zusammenfassung des Hostsystems einschließlich bekannter Werte zeigt.
 * Aufgerufen wird es aus dem Kontextmenü in der Hosttabelle.
 */
class Hostfenster final : Fenster {

public:

    /**
     * Einziger zur Verfügung stehender Konstruktor.
     * @param parent Diesem Objekt wird nach Schließen des Fensters die Event-Kontrolle (zurück-)übergeben.
     * @param host Host, welcher im Fenster betrachtet werden soll.
     */
    Hostfenster(IEventReceiver* parent, Host* host);

private:

    /**
     * Treten Events innerhalb des Fensters auf, wird diese Methode sie verwalten.
     * @param event Passierter Event.
     * @return True, wenn der Event abgearbeitet wurde, false, wenn er weitergereicht wird.
     */
    bool OnEvent(const irr::SEvent& event) override;

    /**
     * Baut das Fenster auf.
     * @return True bei Erfolg.
     */
    bool aufbauen() override;

    /// Enthält die Event-Loop.
    void zeigen() override;

    /// Anzuzeigender Host, darf nicht `nullptr` sein.
    Host* host;

    /// Zeiger zum angezeigten Fenster.
    irr::gui::IGUIWindow* fenster;

};
