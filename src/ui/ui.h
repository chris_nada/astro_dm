#pragma once

#include <irrlicht.h>

/**
 * Enthält und verwaltet den Zugriff auf globale UI- und Rendering-Komponenten.
 */
class UI final {

public:

    /// Initialisiert das UI. Muss vor Anzeigen aller Fenster aufgerufen werden.
    static bool init();

    /// Legt die Transparenz der UI fest.
    static void set_color();

    /// Führt Speicherbereinigung durch. Muss vor Beenden des Programms aufgerufen werden.
    static void finalize() { device->drop(); }

    /// Link zum verwendeten `IrrlichtDevice`.
    static irr::IrrlichtDevice* device;

    /// Link zum verwendeten Irrlicht `IVideoDriver`.
    static irr::video::IVideoDriver* driver;

    /// Link zum verwendeten Irrlicht `IGUIEnvironment`.
    static irr::gui::IGUIEnvironment* guie;

    /// Link zum verwendeten Irrlicht `IGUISkin`.
    static irr::gui::IGUISkin* skin;

    /// Von dem UI verwendete Hintergrundfarbe.
    static irr::video::SColor FARBE_HG;

    /// Schriftart: Konstante Breite, Größe 10.
    static inline irr::gui::IGUIFont* FONT_MONO_10;

    /// Schriftart: Konstante Breite, Größe 12.
    static inline irr::gui::IGUIFont* FONT_MONO_12;

    /// Schriftart: Konstante Breite, Größe 16.
    static inline irr::gui::IGUIFont* FONT_MONO_16;

    /// Schriftart: Serif, Größe 10.
    static inline irr::gui::IGUIFont* FONT_SERIF_10;

    /// Schriftart: Serif, Größe 12.
    static inline irr::gui::IGUIFont* FONT_SERIF_12;

    /// Schriftart: Serif, Größe 16.
    static inline irr::gui::IGUIFont* FONT_SERIF_16;

};
