# Zum Batch-Umbenennen aller Dateien im Verzeichnis
import os
for datei in os.listdir("."):
    if ".txt" in datei:
        os.rename(datei, datei.replace("txt", "xml"))
